﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CommonLibrary
{
    public class DbHelper: IDisposable
    {
        private SqlConnection sqlConn;
        private List<SqlParameter> SqlParameters = new List<SqlParameter>();
        private SqlCommand sqlCmd;
        private bool istranscation = false;
        private int DefaultTimeOut = 600;
        public int TimeOut
        {
            get { return DefaultTimeOut; }
            set { DefaultTimeOut = value; }
        }
        /// <summary>
        /// 启动事务。
        /// </summary>
        public void BeginTransaction()
        {
            try
            {
                if(sqlConn == null)
                {
                    throw new Exception("请先创建连接");
                }
                sqlConn.Open();
                sqlCmd = sqlConn.CreateCommand();
                sqlCmd.Transaction = sqlConn.BeginTransaction();
                istranscation = true;
            }
            catch (Exception e)
            {
                throw new Exception("启动事务失败：" +
                    e.Message);
            }
        }
        public object GetParameterValue(string paraname)
        {
            var p = SqlParameters.FirstOrDefault(x => x.ParameterName == paraname);
            if (p == null)
                return null;
            else
                return p.Value;
        }
        public void RollBackTranscation()
        {
            if (sqlCmd.Transaction == null)
                throw new InvalidOperationException("没有活动的事务需要回滚！");
            sqlCmd.Transaction.Rollback();
            sqlCmd.Transaction = null;
            sqlConn.Close();
        }
        public void CommitTransaction()
        {
            if (sqlCmd.Transaction == null)
                throw new InvalidOperationException("没有活动的事务需要提交！");
            sqlCmd.Transaction.Commit();
            sqlCmd.Transaction = null;
            sqlConn.Close();
        }
        private void BeforeExecute()
        {
            if (sqlConn.State == ConnectionState.Open)
                return;
            else
                sqlConn.Open();
        }
        public void ClearParatmeter()
        {
            sqlCmd.Parameters.Clear();
            SqlParameters.Clear();
        }
        private void EndExecute()
        {
            //sqlCmd.Parameters.Clear();
            //SqlParameters.Clear();
            if (istranscation)
                return;
            else
            {
                sqlCmd.Dispose();
                sqlConn.Close();
            }
        }

        public void AddParameter(string key, object value, ParameterDirection paratype = ParameterDirection.Input)
        {
            if (value == null)
                value = "";
            var p = SqlParameters.FirstOrDefault(x => x.ParameterName == key);
            if (p == null)
            {
                SqlParameter para = new SqlParameter(key, value);
                para.Direction = paratype;
                if(typeof(string) == value.GetType())
                    para.Size = 2147483647;
                SqlParameters.Add(para);
            }
            else
            {
                p.Value = value;
                p.Direction = paratype;
            }
        }
        /// <summary>
        /// 用完之后请释放
        /// </summary>
        public DbHelper()
        {
            ConnectionStringSettings setting = ConfigurationManager.ConnectionStrings["connStr"];
            if (setting == null || String.IsNullOrEmpty(setting.ToString()))
                throw new Exception("未配置名称为connStr的链接字符串，请检查");
            sqlConn = new SqlConnection(setting.ToString());
        }
        /// <summary>
        /// 用完之后请释放
        /// </summary>
        public DbHelper(string connectionString)
        {
            sqlConn = new SqlConnection(connectionString);
        }
        public List<T> ExecuteProcedureAndOutputParamter<T>(string procname, out HashObject outputpara)
        {
            List<T> res = new List<T>();
            DataSet ResponseDs = new DataSet();
            BeforeExecute();
            try
            {
                outputpara = new HashObject();
                if (istranscation)
                    sqlCmd.CommandText = procname;
                else
                    sqlCmd = new SqlCommand(procname, sqlConn) { CommandTimeout = TimeOut};

                sqlCmd.CommandType = CommandType.StoredProcedure;

                foreach (var para in SqlParameters)
                {
                    sqlCmd.Parameters.Add(para);
                }

                SqlDataAdapter sda = new SqlDataAdapter(sqlCmd);

                sda.Fill(ResponseDs);

                foreach (var para in SqlParameters)
                {
                    if (para.Direction == ParameterDirection.Output)
                    {
                        outputpara.Add(para.ParameterName, para.Value);
                    }
                }
                foreach (var item in ResponseDs.Tables)
                {
                    if (typeof(T) == typeof(object))
                    {
                        res.Add((T)CreatNewClassBydt(item as DataTable));
                    }
                    else if (typeof(T) == typeof(List<object>))
                    {
                        res.Add((T)CreatNewListClassBydt(item as DataTable));
                    }
                    else
                    {
                        res.Add((T)item);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                EndExecute();
            }

            return res;
        }

        public int ExecuteIntSQL(string sql)
        {
            int count = 0;
            BeforeExecute();
            if (istranscation)
                sqlCmd.CommandText = sql;
            else
                sqlCmd = new SqlCommand(sql, sqlConn) { CommandTimeout = TimeOut };
            {
                sqlCmd.CommandType = CommandType.Text;

                foreach (var para in SqlParameters)
                {
                    sqlCmd.Parameters.Add(para);
                }

                count = sqlCmd.ExecuteNonQuery();
            }
            EndExecute();
            return count;
        }

        public void ExecuteNonSQL(string sql)
        {
            int count = 0;
            BeforeExecute();
            if (istranscation)
                sqlCmd.CommandText = sql;
            else
                sqlCmd = new SqlCommand(sql, sqlConn) { CommandTimeout = TimeOut };
            {
                sqlCmd.CommandType = CommandType.Text;

                foreach (var para in SqlParameters)
                {
                    sqlCmd.Parameters.Add(para);
                }

                count = sqlCmd.ExecuteNonQuery();
            }
            EndExecute();
        }

        /// <summary>
        /// 执行查询，并返回查询所返回的结果集中第一行的第一列。忽略其他列或行。同 GetValueFromSql
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public object ExecuteSingleSelectValue(string sql)
        {
            object obj;
            BeforeExecute();
            if (istranscation)
                sqlCmd.CommandText = sql;
            else
                sqlCmd = new SqlCommand(sql, sqlConn) { CommandTimeout = TimeOut };
            {
                sqlCmd.CommandType = CommandType.Text;

                foreach (var para in SqlParameters)
                {
                    sqlCmd.Parameters.Add(para);
                }

                obj = sqlCmd.ExecuteScalar();
            }
            EndExecute();
            return obj;
        }

        public T ExecuteProcedureWithOneTable<T>(string procname)
        {
            return GetTables<T>(procname, CommandType.StoredProcedure)[0];
        }

        public List<T> ExecuteProcedure<T>(string procname)
        {
            return GetTables<T>(procname, CommandType.StoredProcedure);
        }

        public T ExecuteSqlWithOneTable<T>(string sql)
        {
            return GetTables<T>(sql, CommandType.Text)[0];
        }

        public List<T> ExecuteSqlTables<T>(string sql)
        {
            return GetTables<T>(sql, CommandType.Text);
        }

        private List<T> GetTables<T>(string textname,CommandType type,bool returnvalue=false)
        {
            DataSet ResponseDs = new DataSet();
            List<T> res = new List<T>();
            BeforeExecute();
            try
            {
                if (istranscation)
                    sqlCmd.CommandText = textname;
                else
                    sqlCmd = new SqlCommand(textname, sqlConn) { CommandTimeout = TimeOut };
                {
                    sqlCmd.CommandType = type;

                    foreach (var para in SqlParameters)
                    {
                        sqlCmd.Parameters.Add(para);
                    }
                    if (returnvalue)
                    {
                        SqlParameter param = sqlCmd.CreateParameter();
                        param.Direction = ParameterDirection.ReturnValue;
                        sqlCmd.Parameters.Add(param);
                    }
                    SqlDataAdapter sda = new SqlDataAdapter(sqlCmd);

                    sda.Fill(ResponseDs);
                    if (returnvalue)
                    {
                        SqlParameters.Add(sqlCmd.Parameters[sqlCmd.Parameters.Count > 0 ? sqlCmd.Parameters.Count - 1 : 0]);
                    }
                }
                
                foreach (var item in ResponseDs.Tables)
                {
                    if (typeof(T) == typeof(object))
                    {
                        res.Add((T)CreatNewClassBydt(item as DataTable));
                    }
                    else if (typeof(T) == typeof(List<object>))
                    {
                        res.Add((T)CreatNewListClassBydt(item as DataTable));
                    }
                    else
                    {
                        res.Add((T)item);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                if(!returnvalue)
                    EndExecute();
            }
            return res;
        }

        /// <summary>
        /// 使用反射 动态创建类，将T的列名动态添加为该类的属性，并给属性赋值
        /// 该方法由于要动态创建类，性能比较低（注意只是将T的第一行转换为动态实体类）
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private Object CreatNewClassBydt(DataTable dt)
        {
            if (dt == null)
            {
                return null;
            }
            //创建编译器实例。
            CSharpCodeProvider provider = new CSharpCodeProvider();
            //设置编译参数。
            CompilerParameters paras = new CompilerParameters();
            paras.GenerateExecutable = false;
            paras.GenerateInMemory = true;
            //创建动态代码。
            StringBuilder classSource = new StringBuilder();
            classSource.Append("public class DynamicClass \n");
            classSource.Append("{\n");
            //创建属性。
            foreach (DataColumn cl in dt.Columns)
            {
                classSource.Append("public string "+(cl.ColumnName)+"{get;set;}");
                classSource.Append("\n");
            }
            classSource.Append("}");
            System.Diagnostics.Debug.WriteLine(classSource.ToString());
            //编译代码。
            CompilerResults result = provider.CompileAssemblyFromSource(paras, classSource.ToString());
            //获取编译后的程序集。
            Assembly assembly = result.CompiledAssembly;
            object obclass = assembly.CreateInstance("DynamicClass");
            foreach (DataColumn cl in dt.Columns)
            {
                ReflectionSetProperty(obclass, cl.ColumnName, dt.Rows[0][cl.ColumnName].ToString());
                PropertyInfo _Property = obclass.GetType().GetProperty(cl.ColumnName);
                if (_Property != null && _Property.CanRead)
                {
                    _Property.SetValue(obclass, dt.Rows[0][cl.ColumnName].ToString(), null);
                }
            }
            return obclass;
        }

        private Object CreatNewListClassBydt(DataTable dt)
        {
            if (dt == null)
            {
                return null;
            }
            //创建编译器实例。
            CSharpCodeProvider provider = new CSharpCodeProvider();
            //设置编译参数。
            CompilerParameters paras = new CompilerParameters();
            paras.GenerateExecutable = false;
            paras.GenerateInMemory = true;
            //创建动态代码。
            StringBuilder classSource = new StringBuilder();
            classSource.Append("public class DynamicClass \n");
            classSource.Append("{\n");
            //创建属性。
            foreach (DataColumn cl in dt.Columns)
            {
                classSource.Append("public string " + (cl.ColumnName) + "{get;set;}");
                classSource.Append("\n");
            }
            classSource.Append("}");
            System.Diagnostics.Debug.WriteLine(classSource.ToString());
            //编译代码。
            CompilerResults result = provider.CompileAssemblyFromSource(paras, classSource.ToString());
            //获取编译后的程序集。
            Assembly assembly = result.CompiledAssembly;
            List<object> list = new List<object>();
            foreach (var row in dt.Rows)
            {
                var r = row as DataRow;
                object obclass = assembly.CreateInstance("DynamicClass");
                foreach (DataColumn cl in dt.Columns)
                {
                    ReflectionSetProperty(obclass, cl.ColumnName, r[cl.ColumnName].ToString());
                    PropertyInfo _Property = obclass.GetType().GetProperty(cl.ColumnName);
                    if (_Property != null && _Property.CanRead)
                    {
                        _Property.SetValue(obclass, r[cl.ColumnName].ToString(), null);
                    }
                }
                list.Add(obclass);
            }
            return list;
        }
        private void ReflectionSetProperty(object objClass, string propertyName, string value)
        {
            PropertyInfo[] infos = objClass.GetType().GetProperties();
            foreach (PropertyInfo info in infos)
            {
                if (info.Name == propertyName && info.CanWrite)
                {
                    info.SetValue(objClass, value, null);
                }
            }
        }
        public int ExecuteProcedureReturnValue(string procname)
        {
            GetTables<DataTable>(procname, CommandType.StoredProcedure,true);
            int res = Convert.ToInt32(SqlParameters[SqlParameters.Count - 1].Value);
            EndExecute();
            return res;
        }
        /// <summary>
        /// 同 ExecuteSingleSelectValue。一般用于判断是否存在 select 1 or select 0
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public int GetValueFromSql(string sql)
        {
            object answer;
            BeforeExecute();
            if (istranscation)
                sqlCmd.CommandText = sql;
            else
                sqlCmd = new SqlCommand(sql, sqlConn);
            {
                sqlCmd.CommandType = CommandType.Text;

                foreach (var para in SqlParameters)
                {
                    sqlCmd.Parameters.Add(para);
                }

                answer = sqlCmd.ExecuteScalar();
            }
            EndExecute();

            return answer != DBNull.Value ? Convert.ToInt32(answer) : 0;
        }

        public void Dispose()
        {
            if (sqlConn.State == ConnectionState.Open)
                sqlConn.Close();
            if (sqlCmd != null)
                sqlCmd.Dispose();
        }
    }
}
