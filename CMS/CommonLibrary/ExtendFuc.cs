﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

namespace CommonLibrary
{
    /// <summary>
    /// 扩展类
    /// </summary>
    public static class ExtendFuc
    {
        #region Json 字符串 转换为 DataTable数据集合
        /// <summary>
        /// Json 字符串 转换为 DataTable数据集合
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static DataTable ToDataTable(this string json)
        {
            DataTable dataTable = new DataTable();  //实例化
            DataTable result;
            try
            {
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                javaScriptSerializer.MaxJsonLength = Int32.MaxValue; //取得最大数值
                ArrayList arrayList = javaScriptSerializer.Deserialize<ArrayList>(json);
                if (arrayList.Count > 0)
                {
                    foreach (Dictionary<string, object> dictionary in arrayList)
                    {
                        if (dictionary.Keys.Count<string>() == 0)
                        {
                            result = dataTable;
                            return result;
                        }
                        if (dataTable.Columns.Count == 0)
                        {
                            foreach (string current in dictionary.Keys)
                            {
                                dataTable.Columns.Add(current, dictionary[current].GetType());
                            }
                        }
                        DataRow dataRow = dataTable.NewRow();
                        foreach (string current in dictionary.Keys)
                        {
                            dataRow[current] = dictionary[current];
                        }

                        dataTable.Rows.Add(dataRow); //循环添加行到DataTable中
                    }
                }
            }
            catch
            {
            }
            result = dataTable;
            return result;
        }
        #endregion

        #region DataTable 转换为Json 字符串
        /// <summary>
        /// DataTable 对象 转换为Json 字符串
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToJson(this DataTable dt)
        {
            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
            javaScriptSerializer.MaxJsonLength = Int32.MaxValue; //取得最大数值
            ArrayList arrayList = new ArrayList();
            foreach (DataRow dataRow in dt.Rows)
            {
                Dictionary<string, object> dictionary = new Dictionary<string, object>();  //实例化一个参数集合
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    if(dataRow[dataColumn.ColumnName] != null &&dataRow[dataColumn.ColumnName].GetType() == typeof(DateTime))
                        dictionary.Add(dataColumn.ColumnName, Convert.ToDateTime(dataRow[dataColumn.ColumnName]).ToString("yyyy-MM-dd HH:mm:ss"));
                    else
                        dictionary.Add(dataColumn.ColumnName, dataRow[dataColumn.ColumnName].ToString());
                }
                arrayList.Add(dictionary); //ArrayList集合中添加键值
            }

            return javaScriptSerializer.Serialize(arrayList);  //返回一个json字符串
        }

        public static string ToJsonLower(this DataTable dt)
        {
            string x = ToJson(dt);
            return string.IsNullOrEmpty(x) ? "" : x.ToLower();
        }
        #endregion

        public static List<HashObject> ToHashObjectList(this DataTable dt)
        {
            List<HashObject> result = new List<HashObject>();
            foreach (DataRow dataRow in dt.Rows)
            {
                HashObject ho = new HashObject();
                Dictionary<string, object> dictionary = new Dictionary<string, object>();  //实例化一个参数集合
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    if (dataRow[dataColumn.ColumnName] != null && dataRow[dataColumn.ColumnName].GetType() == typeof(DateTime))
                        ho.Add(dataColumn.ColumnName, Convert.ToDateTime(dataRow[dataColumn.ColumnName]).ToString("yyyy-MM-dd HH:mm:ss"));
                    else
                        ho.Add(dataColumn.ColumnName, dataRow[dataColumn.ColumnName].ToString());
                }
                result.Add(ho);
            }
            return result;
        }

        public static HashObject ToHashObject(this DataRow dataRow)
        {
            if (dataRow == null)
                throw new Exception("DataRow为空");
            HashObject ho = new HashObject();
            Dictionary<string, object> dictionary = new Dictionary<string, object>();  //实例化一个参数集合
            foreach (DataColumn dataColumn in dataRow.Table.Columns)
            {
                if (dataRow[dataColumn.ColumnName] != null && dataRow[dataColumn.ColumnName].GetType() == typeof(DateTime))
                    ho.Add(dataColumn.ColumnName, Convert.ToDateTime(dataRow[dataColumn.ColumnName]).ToString("yyyy-MM-dd HH:mm:ss"));
                else
                    ho.Add(dataColumn.ColumnName, dataRow[dataColumn.ColumnName].ToString());
            }
            return ho;
            
        }

        /// <summary>
        /// 该方法内值需要保证为基本类型（int，string，double，float，bool等），
        /// </summary>
        /// <param name="para"></param>
        /// <returns></returns>
        public static string ToQueryNameValueString(this HashObject para)
        {
            string result = "";
            foreach(var key in para.Keys)
            {
                if (para[key] != null)
                {
                    if (para[key].GetType() == typeof(int) || para[key].GetType() == typeof(string)|| para[key].GetType() == typeof(double) || para[key].GetType() == typeof(float) || para[key].GetType() == typeof(bool))
                    {
                        result += key + "=" + replacespecialchar(para[key].ToString()) + "&";
                    }
                    else
                    {
                        result += key + "=" + replacespecialchar(para[key].ToString()) + "&";
                    }
                }
            }
            if (result.Length > 0)
                result = result.Substring(0, result.Length - 1);
            return result;
        }
        private static string replacespecialchar(string value)
        {
            return value;
        }

    }
}
