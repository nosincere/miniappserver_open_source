﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class WxConstObject
    {
        /// <summary>
        /// 微信小程序统一下单接口，v3版本
        /// </summary>
        public const string unifiedorder_v3 = "https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi";

        /// <summary>
        /// 统一下单
        /// </summary>
        public const string unifiedorder_v2 = "https://api.mch.weixin.qq.com/pay/unifiedorder";
        public const string orderquery = "https://api.mch.weixin.qq.com/pay/orderquery";


        public const string store_name = "艺顺翔名酒行";

        /// <summary>
        /// 中国,甘肃省,定西市,临洮县
        /// </summary>
        public const string store_area_code = "621124";

        /// <summary>
        /// 
        /// </summary>
        public const string store_address = "甘肃省定西市临洮县新街55号";
    }
}
