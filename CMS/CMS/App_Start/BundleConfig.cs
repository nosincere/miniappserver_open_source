﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace CMS.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquerytwo").Include(
                        "~/Scripts/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/base").Include(
                        "~/Scripts/base.js",
                        "~/Scripts/xtiper.js"));
            bundles.Add(new ScriptBundle("~/bundles/hplusjs").Include(
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/plugins/metisMenu/jquery.metisMenu.js",
                        "~/Scripts/plugins/slimscroll/jquery.slimscroll.min.js",
                        "~/Scripts/plugins/layer/layer.min.js",
                        "~/Scripts/hplus.min.js",
                        "~/Scripts/contabs.js",
                        "~/Scripts/plugins/pace/pace.min.js"
                        ));
            //bundles.Add(new StyleBundle("~/bundles/hpluscss").Include(
            //    "~/css/font-awesome.min.css",
            //    "~/css/animate.css",
            //    "~/Js/plugins/layer/skin/layer.css",
            //    "~/css/style.css"
            //    ));
            bundles.Add(new StyleBundle("~/bundles/tablelayoutcss").Include(
                "~/Content/css2/app.v2.css",
                "~/Content/css2/style.css",
                "~/Content/css/font-awesome.min93e3.css",
                "~/Content/css/animate.min.css",
                "~/Content/css2/mystyle.css",
                "~/Scripts/plugins/bootstrap-table/bootstrap-table.css",
                "~/Scripts/plugins/bootstrap-table/bootstrap-editable.css",
                "~/Scripts/plugins/bootstrap-table/typeaheadjs/typeahead.js-bootstrap.css",
                "~/Content/css/plugins/toastr/toastr.min.css",
                "~/Content/css/xtiper.css"
                ));

        }
    }
}