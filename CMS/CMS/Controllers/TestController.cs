﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public JsonResult Index()
        {
            int orderid = Convert.ToInt32(Request.Params["id"]);
            WxPayObjectV2 pay = new WxPayObjectV2(orderid);

            var result = pay.ToPay();
            return Json(result,JsonRequestBehavior.AllowGet);
        }
    }
}