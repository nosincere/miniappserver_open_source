﻿using CommonLibrary;
using Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class PointsDal
    {
        /// <summary>
        /// 小程序签到
        /// </summary>
        /// <param name="APhonePubParam"></param>
        /// <returns></returns>
        public static TPhoneRetParam WxMiniAppUserSignIn(TPhonePubParam APhonePubParam)
        {
            TPhoneRetParam result = new TPhoneRetParam();

            using (DbHelper db = new DbHelper())
            {
                db.AddParameter("@token", APhonePubParam.Data.GetValue<string>("token", ""));
                DataTable dt = db.ExecuteProcedureWithOneTable<DataTable>("P_WxMiniAppUserSignIn");
                result.RetCode = 0;
                result.RetJson = dt.Rows[0]["Result"].ToString();
                result.data = JsonConvert.DeserializeObject(result.RetJson);
                return result;
            }
        }
        /// <summary>
        /// 获取签到记录
        /// </summary>
        /// <param name="APhonePubParam"></param>
        /// <returns></returns>
        public static TPhoneRetParam WxMiniAppGetUserSignRecord(TPhonePubParam APhonePubParam)
        {
            TPhoneRetParam result = new TPhoneRetParam();

            using (DbHelper db = new DbHelper())
            {
                db.AddParameter("@token", APhonePubParam.Data.GetValue<string>("token", ""));
                db.AddParameter("@datestr", APhonePubParam.Data.GetValue<string>("datestr"));
                DataTable dt = db.ExecuteProcedureWithOneTable<DataTable>("P_WxMiniAppGetUserSignRecord");
                result.RetCode = 0;
                result.RetJson = dt.Rows[0]["Result"].ToString();
                result.data = JsonConvert.DeserializeObject(result.RetJson);
                return result;
            }
        }

        /// <summary>
        /// 获取签到记录
        /// </summary>
        /// <param name="APhonePubParam"></param>
        /// <returns></returns>
        public static TPhoneRetParam WxMiniAppUserPointsLog(TPhonePubParam APhonePubParam)
        {
            TPhoneRetParam result = new TPhoneRetParam();

            using (DbHelper db = new DbHelper())
            {
                db.AddParameter("@token", APhonePubParam.Data.GetValue<string>("token", ""));
                DataTable dt = db.ExecuteProcedureWithOneTable<DataTable>("P_WxMiniAppUserPointsLog");
                result.RetCode = 0;
                result.RetJson = dt.Rows[0]["Result"].ToString();
                result.data = JsonConvert.DeserializeObject(result.RetJson);
                return result;
            }
        }
    }
    
}
