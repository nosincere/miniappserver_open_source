﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonLibrary
{
    public class HashObject 
    {
        private Dictionary<string, object> dic = new Dictionary<string, object>();

        public void Add(string key, object value)
        {
            dic.Add(key, value);
        }

        public T GetValue<T>(string key, T defaultvalue = default(T))
        {
            object value;
            if (dic.TryGetValue(key, out value))
            {
                Type t = typeof(T);
                if (t.IsPrimitive || t == typeof(bool) || t == typeof(string) || t == typeof(DateTime) || t == typeof(decimal))
                {
                    if (value == null || value.ToString() == "")
                        return default(T);
                    else
                        return (T)Convert.ChangeType(value, t, null);
                }
                else if (t == typeof(int))
                {
                    if (value == null)
                        return default(T);
                    else
                        return (T)value;
                }
                else
                    return (T)value;
            }
            else
                return defaultvalue;
        }

        public bool Remove(string key)
        {
            if (dic.Keys.Contains(key))
            {
                return dic.Remove(key);
            }
            else
                return false;
        }
        public bool ContainsKey(string key)
        {
            return dic.Keys.Contains(key);
        }
        public List<string> Keys
        {
            get
            {
                return dic.Keys.ToList();
            }
        }
        public object this[string key]
        {
            get
            {
                try
                {
                    object v = null;
                    if (ContainsKey(key)){

                        v = GetValue<object>(key);
                    }
                    return v;
                }
                catch (KeyNotFoundException)
                {
                    throw new KeyNotFoundException(string.Format("关键字“{0}”不在HashObject中。", key));
                }
            }
            set
            {
                Add(key, value);
            }
        }
        public void CopyTo(IDictionary<string, object> dict)
        {
            foreach (KeyValuePair<string, object> pair in dic)
            {
                dict[pair.Key] = pair.Value;
            }
        }
    }
}
