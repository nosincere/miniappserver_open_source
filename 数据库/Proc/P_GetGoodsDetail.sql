If OBJECT_ID('P_GetGoodsDetail') Is Not Null
	Drop Procedure P_GetGoodsDetail
Go

Create Procedure P_GetGoodsDetail
(
	@goodsid			nvarchar(200),
	@token			nvarchar(200)
)
As
Begin
		
	Declare @ResultData nvarchar(max),@detail nvarchar(max),@content nvarchar(max),@detailspec nvarchar(max),@image nvarchar(max), @specData nvarchar(max),
		@spec_items nvarchar(max),@carttotal int,@userid nvarchar(20),@goodsname nvarchar(200),@comment nvarchar(max),@goodssalecount int
	Select @userid = dbo.GetUserId(@token)

	Select @content = Content,@goodsname = Title From T_Goods where Id = @goodsid
	--select * from T_GoodsDetail where GoodsId = @goodsid order by Price
	Select @detailspec = ''
	Select @detailspec = @detailspec + 
		'{'+
			'"spec_sku_id":'+ Cast(Id As nvarchar) + ',' +
			'"goods_price":'+ Cast(price As nvarchar) + ',' +
			'"line_price":'+ Cast(costprice As nvarchar) + ',' +
			'"stock_num":'+ Cast(KuCun As nvarchar) + 
		'},'
		From T_GoodsDetail where GoodsId = @goodsid order by Price
	Select @detailspec = ISNULL(@detailspec,'')
	IF LEN(@detailspec) > 0
		Select @detailspec = LEFT(@detailspec,LEN(@detailspec)-1)
	Select @image=''
	Select @image = @image +
		'{'+
			'"file_path":"'+ImgUrl+'"'+
		'},'
		From T_GoodsBannerImage Where GoodsId = @goodsid 
		Order by iorder
	IF LEN(@image) > 0
		Select @image = LEFT(@image,LEN(@image)-1)	
	Select @image = ISNULL(@image,'')
	
	Select top 5 u.avatarUrl,u.nickName,c.Stars,c.Comment,c.CreateTime,img.ImgUrl,gd.Name
	Into #comment
	From T_OrderDetailComment c
		Inner Join T_Goods g on c.GoodsId = g.Id
		Inner Join T_GoodsDetail gd on c.GoodsSkuId = gd.Id
		Inner Join T_WxUser u on c.WxUserId = u.[user_id]
		Left Join (SELECT OrderDetailId, STUFF(
			( SELECT ',"'+ ImgUrl +'"'
			FROM T_CommentImage b 
			WHERE b.OrderDetailId = a.OrderDetailId 
			FOR XML PATH('')),1 ,1, '') ImgUrl
			from T_CommentImage a
			group by OrderDetailId
			) img on c.OrderDetailId = img.OrderDetailId
		where c.GoodsId = @goodsid
		Order By c.CreateTime desc
	
	select @comment = ''
	select @comment = @comment + '{' +
			'"skuname":"'+Name+'",'+
			'"avatar":"'+avatarUrl+'",'+
			'"nickname":"'+nickName+'",'+
			'"stars":'+cast(Stars as nvarchar)+','+
			'"comment":"'+Comment+'",'+
			'"createtime":"'+CONVERT(varchar(100), CreateTime, 23) + ' ' + CONVERT(varchar(100), CreateTime, 108)+'",'+
			'"imgurl":['+ISNULL(ImgUrl,'')+']'+
		'},'
		From #comment
		
	IF LEN(@comment) > 0
		Select @comment = LEFT(@comment,LEN(@comment)-1)	
	Select @comment = ISNULL(@comment,'')
	
	select @goodssalecount = COUNT(*) From T_OrderDetail where GoodsId = @goodsid
	
	Select @detail = '{"goods_name":"'+@goodsname+'","spec_type":20,"goods_id":"'+@goodsid+'","content":"'+@content+'","image":['+@image+'],"spec":['+@detailspec+'],"goods_sales":'+CAST(@goodssalecount as nvarchar)+'}'		

	select @specData = ''	
	select @spec_items = ''
	Select @spec_items = @spec_items +
		'{'+
			'"item_id":'+CAST(Id as nvarchar)+','+
			'"spec_sku_id":'+CAST(Id as nvarchar)+','+
			'"goods_price":'+CAST(Price as nvarchar)+','+
			'"line_price":'+CAST(costprice as nvarchar)+','+
			'"stock_num":'+ Cast(KuCun As nvarchar) + ','+
			'"checked":'+case when LEN(@spec_items) = 0 then 'true' else 'false' end+','+
			'"spec_value":"'+Name+'"' +
		'},'		
		From T_GoodsDetail where GoodsId = @goodsid order by Price 
	IF LEN(@spec_items) > 0
		Select @spec_items = LEFT(@spec_items,LEN(@spec_items)-1)	
	Select @carttotal = ISNULL(SUM(Number),0) From T_CartList where UserId=@userid
	Select @specData = '"spec_attr":[{"group_id":"'+@goodsid+'","group_name":"�������","spec_items":['+@spec_items+']}],"spec_list":['+@spec_items+']'
	Select @ResultData = '{"detail":' + @detail + ',"comment":['+@comment+'],"specData":{'+@specData+'},"showView":true,"cart_total_num":'+CAST(@carttotal as nvarchar)+'}'
	Select @ResultData Result
End
Go

--exec P_GetGoodsDetail 1
--select * from T_Goods
--select * from T_GoodsDetail