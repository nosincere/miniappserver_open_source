IF OBJECT_ID('P_SaveCategory') IS Not Null
	Drop Procedure P_SaveCategory
Go
Create procedure P_SaveCategory
(
	@Id			int,
	@ParId		int,
	@ImgUrl		nvarchar(500),
	@Name		nvarchar(500),
	@GoodsIds	nvarchar(500),
	@OrderIndex	int,
	@ShowInIndexPage int,
	@IgnoreCheck int,
	@msg		nvarchar(100) output
)
As
Begin
	select @msg = ''
	
	if @IgnoreCheck <> 1
	begin
		declare @indexcount int
		select @indexcount = count(1) From T_Category where ShowInIndexPage = 1 and id <> @Id
		select @indexcount = @indexcount + @ShowInIndexPage
		if @indexcount > 4
		begin
			Select @msg ='此操作将造成首页显示分类数大于4，是否继续？'
			return
		end
	end
	
	If ISNULL(@Id,0) <> 0
		Update T_Category set Name = @Name,ImgUrl = @ImgUrl,OrderIndex = @OrderIndex,GoodsIds=@GoodsIds,ShowInIndexPage=@ShowInIndexPage Where Id = @Id
	Else
	Begin
		Declare @CurrentLeveal Int
		If @ParId = 0
			Select @CurrentLeveal = 1
		Else
			Select @CurrentLeveal = Leveal + 1 From T_Category Where Id = @ParId
		Insert Into T_Category(Name,ImgUrl,OrderIndex,ParId,GoodsIds,ShowInIndexPage, Leveal)
		Select @Name,@ImgUrl,@OrderIndex,@ParId,@GoodsIds,@ShowInIndexPage, @CurrentLeveal
	End
End
Go
