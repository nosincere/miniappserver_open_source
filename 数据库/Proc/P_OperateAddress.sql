
if exists (select * from dbo.sysobjects where id = object_id(N'P_OperateAddress'))
	drop procedure P_OperateAddress
GO

Create Procedure P_OperateAddress
(
	@mode			nvarchar(10),		--A 添加，M 修改，D 删除
	@Id				int,
	@token			nvarchar(200),
	@Person			nvarchar(100),
	@Tel			nvarchar(60),
	@Mobile			nvarchar(20),
	@Province		nvarchar(20),
	@City			nvarchar(20),
	@County			nvarchar(20),
	@Address		nvarchar(256),
	@Default		int
)
As
Begin
	declare @ret int
	select @ret=0
	Declare @ResultData nvarchar(max),@list nvarchar(max),@userid nvarchar(20)
	Select @userid = dbo.GetUserId(@token)
	if @mode = 'A'
	begin
		if @Default = 1 and exists(select 1 from [T_ShoppingAddress] where [cUserId]=@userid and bDefault=1)
			update [T_ShoppingAddress] set bDefault=0 where [cUserId]=@userid and bDefault = 1
		
		insert into [T_ShoppingAddress]([cUserId],cPerson,cTelephone,cMobile,cProvince,cCity,cCounty,cAddress,bDefault)
		select @userid,@Person,@Tel,@Mobile,@Province,@City,@County,@Address,@Default
		
		declare @defaultcount int
		select @defaultcount = COUNT(*) From [T_ShoppingAddress] where cUserId = @userid
		if @defaultcount = 1
			Update [T_ShoppingAddress] set bDefault = 1 where cUserId = @userid
		
		Select '添加成功' Result
	End
	else if @mode='M'
	begin
		if not exists(select 1 from [T_ShoppingAddress] o 
				inner join T_WxUser u on o.[cUserId] = u.[user_id]
				where u.[user_id] = @userid
					And o.Id = @Id)
		Begin
			set @ret = -1
			return
		End
		if not exists(select 1 from [T_ShoppingAddress] where Id=@Id)
			set @ret = -1
		else
			update [T_ShoppingAddress] set [cUserId]=@userid,cPerson=@Person,cTelephone=@Tel,cMobile=@Mobile,cProvince=@Province,cCity=@City,cCounty=@County,cAddress=@Address where Id=@Id
			
		Select '修改成功' Result
	end
	else if @mode='D'
	begin
		if not exists(select 1 from [T_ShoppingAddress] o 
				inner join T_WxUser u on o.[cUserId] = u.[user_id]
				where u.[user_id] = @userid
					And o.Id = @Id)
		Begin
			set @ret = -1
			return
		End
		if not exists(select 1 from [T_ShoppingAddress] where Id=@Id)
			set @ret = -1
		else
			delete from [T_ShoppingAddress] where Id=@Id
		Select '删除成功' Result
	end
	Else If @mode='Default'
	Begin
		if not exists(select 1 from [T_ShoppingAddress] o 
				inner join T_WxUser u on o.[cUserId] = u.[user_id]
				where u.[user_id] = @userid
					And o.Id = @Id)
		Begin
			set @ret = -1
			return
		End
		Update [T_ShoppingAddress] set bDefault = 1 where Id = @Id
		Update [T_ShoppingAddress] set bDefault = 0 where Id <> @Id And [cUserId]=@userid
	End
	return @ret
End
Go