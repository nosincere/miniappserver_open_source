--�����Ż�ȯ
If OBJECT_ID('P_GenerateCoupon') Is Not Null
	Drop Procedure P_GenerateCoupon
Go
Create Procedure P_GenerateCoupon
(
	@title nvarchar(20),
	@id int,
	@amount	numeric(18,2),
	@limitamount	numeric(18,2),
	@totalcount	int,
	@overdate	nvarchar(20),
	@xml	nvarchar(4000),
	@msg		nvarchar(4000) output
)
AS
Begin
	select @msg=''
	if ISNULL(@id,0)<>0
	begin
		update T_Coupon set Title=@title,Amount=@amount,TotalCount=@totalcount,OverDate=@overdate,LimitAmount=@limitamount,RemainCount = RemainCount + @totalcount - TotalCount
		where Id = @id
	end
	else
	begin
		Declare @couponid int
		Insert Into T_Coupon(Title,Amount,TotalCount,RemainCount,CreateDate,OverDate,LimitAmount)
		Select @Title,@Amount,@TotalCOunt,@TotalCOunt,GETDATE(),@OverDate,@limitamount
		Select @couponid = @@IDENTITY
		
		DECLARE @xmlHandle INT    
		EXEC sp_xml_preparedocument @xmlHandle OUTPUT, @xml   
		SELECT  GoodsId,SkuId
		INTO    #temp
		FROM    OPENXML(@xmlHandle,N'/Root/Record')   
		WITH( GoodsId NVARCHAR(100), SkuId NVARCHAR(4000))
		EXEC sp_xml_removedocument @xmlHandle
		
		Insert Into T_CouponLimit(CouponId,GoodsId,SkuId)
		Select @couponid,GoodsId,SkuId
		from #temp 
	end
End
Go