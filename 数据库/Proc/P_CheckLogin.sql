if exists (select * from dbo.sysobjects where id = object_id(N'P_CheckLogin'))
	drop procedure P_CheckLogin
GO

Create Procedure P_CheckLogin
(
	@UserCode		nvarchar(200),
	@PassWord	nvarchar(200),
	@ReturnCode	int output	
)
As
Begin
	set @ReturnCode = 0
	if not exists(select 1 from T_LoginUser where UserCode=@UserCode and [PassWord]=@PassWord)
	begin
		set @ReturnCode = -1
		select '-1' as errcode,'�˻����������' as errmsg
	end
	else
	begin
		set @ReturnCode = 1
		select Id,UserCode,Name,IsManager,UType,WeChatOpenId from T_LoginUser
	end
End
Go