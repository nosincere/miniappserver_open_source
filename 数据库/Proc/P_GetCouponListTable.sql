If OBJECT_ID('P_GetCouponListTable') Is Not Null
	Drop Procedure P_GetCouponListTable
Go
Create Procedure P_GetCouponListTable
(
	@pageSize	int,
	@pageIndex	int,
	@sort		nvarchar(500),
	@order		nvarchar(500),
	@filter		nvarchar(500),
	@total		int output
)
As
Begin
	Select c.Id, c.Title,c.TotalCount,c.RemainCount,c.Amount,c.LimitAmount,
	CONVERT(varchar(100), CreateDate, 23)+ ' ' + CONVERT(varchar(100), CreateDate, 108) as CreateDate,
	CONVERT(varchar(100), OverDate, 23)+ ' ' + CONVERT(varchar(100), OverDate, 108) as OverDate,
	c.IsStopUsing,case c.IsStopUsing when 1 then '��' else '��' end as IsStopUsingText
	Into #temp
	From T_Coupon c
	declare @records int
	
	exec P_ToPage @TableName ='#temp',@searchField='Title,TotalCount,RemainCount,Amount,LimitAmout',@searchString=@filter,@searchOper='in',@PageSize=@pageSize,@PageIndex=@pageIndex,
		@PageTotal=@records output,@RecordTotal=@total output, @OrderByField=@sort,@OrderByType=@order
End
Go