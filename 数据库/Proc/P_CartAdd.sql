If OBJECT_ID('P_CartAdd') Is Not Null
	Drop Procedure P_CartAdd
Go

Create Procedure P_CartAdd
(
	@token			nvarchar(200),
	@goodsid		int,
	@skuid			int,
	@num			int
)
As
Begin
		
	Declare @ResultData nvarchar(max),@price	numeric(10,2),@userid nvarchar(20),@count int,@retmsg nvarchar(200),@carttotal int

	Select @userid = dbo.GetUserId(@token)
	
	Select @price = Price from T_GoodsDetail where Id = @skuid
	
	If exists(Select 1 From T_CartList Where UserId=@userid And GoodsId = @goodsid And GoodsSkuId=@skuid And AddPrice=@price)
		Update T_CartList Set Number = Number + @num Where UserId=@userid And GoodsId = @goodsid And GoodsSkuId=@skuid And AddPrice=@price
	Else
		Insert Into T_CartList(UserId,GoodsId,GoodsSkuId,AddPrice,Number)
		Select @userid,@goodsid,@skuid,@price,@num
					
	Select @count = @@ROWCOUNT
	if @count = 1
		select @retmsg = '���ӳɹ�' 
	else
		select @retmsg = '����ʧ��'
		
	Select @carttotal = SUM(Number) From T_CartList where UserId=@userid
	Select @ResultData = '{"cart_total_num":'+CAST(@carttotal as nvarchar)+'}'
	Select @ResultData Result,@retmsg RetMsg
End
Go

