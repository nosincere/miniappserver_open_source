If OBJECT_ID('P_BeforeOrderCartCheck') Is Not Null
	Drop Procedure P_BeforeOrderCartCheck
Go

Create Procedure P_BeforeOrderCartCheck
(
	@token			nvarchar(200)
)
As
Begin
		
	Declare @ResultData nvarchar(max),@checkedcount int,@errmsg nvarchar(200)
	Select @token = dbo.GetUserId(@token)
	select @checkedcount = COUNT(*) From T_CartList where UserId = @token and checked=1
	if ISNULL(@checkedcount,0)=0
		select @errmsg = '����ѡ����Ʒ'
	else
		select @errmsg = ''
	Select @ResultData = '{"errmsg":"'+@errmsg+'"}'
	Select @ResultData Result
End
Go
