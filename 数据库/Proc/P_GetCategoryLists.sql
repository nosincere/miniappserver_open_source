If OBJECT_ID('P_GetCategoryLists') Is Not Null
	Drop Procedure P_GetCategoryLists
Go

Create Procedure P_GetCategoryLists
As
Begin

	Declare @ResultJosn NVARCHAR(MAX),@list nvarchar(max),@item nvarchar(max),@showsingle int
	select @showsingle = COUNT(1)  From T_Category where ParId = 0
	--select @showsingle = 1
	Select @list = '['
	DECLARE myCursor Cursor
	FOR (select Id,Name from T_Category Where parid=0 )
	OPEN myCursor
	DECLARE @Id int, @Name nvarchar(20)
	FETCH NEXT FROM myCursor INTO @Id,@Name
	WHILE @@FETCH_STATUS=0
	BEGIN
		Select @item = ''
		Select @list = @list +
			'{'+
				'"name":"'+@Name+'",'+
				'"category_id":"'+Cast(@Id AS Nvarchar)+'",'+
				'"index":"'+Cast(@Id AS Nvarchar)+'",'+
				'"child":['
		Select @item = @item +
			'{'+
				'"category_id":"'+Cast(Id AS Nvarchar)+'",'+
				'"file_id":"'+Cast(Id AS Nvarchar)+'",'+
				'"imgurl":"'+ImgUrl+'",'+
				'"name":"'+Name+'"'+
			'},'
		From T_Category Where ParId = @Id
		Order By OrderIndex asc
		
		If LEN(@item)>1
			Select @item = LEFT(@item,LEN(@item) - 1)
		Select @list = @list + @item +']},'
		Print @list
		FETCH NEXT FROM myCursor INTO @Id,@Name
	END
	CLOSE myCursor
	DEALLOCATE myCursor
	If LEN(@list)>1
		Select @list = LEFT(@list,LEN(@list) - 1)
	Select @list = @list + ']'
	
	Select @ResultJosn=''
	
	Select @ResultJosn = '{"list":' + @list + ',"showsingle":'+CAST(@showsingle as nvarchar)+'}'
	Select @ResultJosn 'Result'
End
Go

