
if exists (select * from dbo.sysobjects where id = object_id(N'P_GetAddressList'))
	drop procedure P_GetAddressList
GO

Create Procedure P_GetAddressList
(
	@token			nvarchar(200)
)
As
Begin
	Declare @ResultData nvarchar(max),@list nvarchar(max),@userid nvarchar(20),@defaultid int
	Select @userid = dbo.GetUserId(@token)
	Select @list = ''
	Select @list =@list + '{'+
					'"name":"'+cPerson+'",'+
					'"phone":"'+cTelephone+'",'+
					'"province":"'+cProvince+'",'+
					'"city":"'+cCity+'",'+
					'"region":"'+cCounty+'",'+
					'"detail":"'+cAddress+'",'+
					'"address_id":"'+CAST(Id as nvarchar)+'"'+
					'},'
	from [T_ShoppingAddress] where [cUserId] = @userid order by [bDefault] desc,[dAddTime] desc
	IF LEN(@list) > 0
		Select @list = LEFT(@list,LEN(@list)-1)
	Select @list = '['+@list+']'
	
	Select @defaultid = Id from [T_ShoppingAddress] where [cUserId] = @userid and bDefault=1 
	
	Select @ResultData = '{"list":'+@list+',"default_id":'+CAST(ISNULL(@defaultid,0) as nvarchar)+'}' 
	Select @ResultData Result
End
Go

