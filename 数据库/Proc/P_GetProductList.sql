IF OBJECT_ID('P_GetProductList') IS Not Null
	Drop Procedure P_GetProductList
Go
Create procedure P_GetProductList
(
	@pageSize	int,
	@pageIndex	int,
	@sort		nvarchar(500),
	@order		nvarchar(500),
	@filter		nvarchar(500),
	@isdisplay	int,
	@total		int output
)
As
Begin
	Select Id as Id,Title,ProPerty,
		CONVERT(varchar(100), dAddTime, 23) + ' ' + CONVERT(varchar(100), dAddTime, 108) dAddTime,
		case bDisplay when 1 then CONVERT(varchar(100), dDisplayTime, 23)+ ' ' + CONVERT(varchar(100), dDisplayTime, 108) else '' end dDisplayTime,
		iOrder,case bDisplay when 1 then '��' else '��' end as bDisplay,cast(bDisplay as int) bDisplayValue
	into #temp
	from T_Goods 
	where ((@isdisplay = 1 And bDisplay = 1) or (@isdisplay = 0 and bDisplay = 0) or @isdisplay=2)
	order by iOrder
	declare @records int
	
	exec P_ToPage @TableName ='#temp',@searchField='Title',@searchString=@filter,@searchOper='in',@PageSize=@pageSize,@PageIndex=@pageIndex,
		@PageTotal=@records output,@RecordTotal=@total output, @OrderByField=@sort,@OrderByType=@order
End
Go