
IF OBJECT_ID('P_GetUserOrderGoodsComment') IS Not Null
	Drop Procedure P_GetUserOrderGoodsComment
Go
Create procedure P_GetUserOrderGoodsComment
(
	@token		nvarchar(50),
	@orderdetailid	int
)
As
Begin
	declare @result nvarchar(max),@imgurl nvarchar(max)
	
	select @result=''
	
	select @result = '"flag":'+CAST(Stars as nvarchar)+',' + 
		'"inputtext":"'+Comment+'",'
		From T_OrderDetailComment
		where OrderDetailId = @orderdetailid
		
	select @imgurl=''
	
	select @imgurl =@imgurl + ''+
			'"'+ImgUrl+'"'+
		','
		From T_CommentImage
		where OrderDetailId = @orderdetailid
		Order by iOrder
	if LEN(@imgurl) > 0
		select @imgurl = LEFT(@imgurl,LEN(@imgurl)-1)
	
	select @result = '{' + @result + '"tempFilePaths":['+@imgurl+'],"showupload":"c-hide"}'
	
	select @result Result
End
Go
