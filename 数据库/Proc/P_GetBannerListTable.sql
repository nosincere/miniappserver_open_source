IF OBJECT_ID('P_GetBannerListTable') IS Not Null
	Drop Procedure P_GetBannerListTable
Go
Create procedure P_GetBannerListTable
(
	@pageSize	int,
	@pageIndex	int,
	@sort		nvarchar(500),
	@order		nvarchar(500),
	@filter		nvarchar(500),
	@total		int output
)
As
Begin

	Select c.imgUrl as ImgUrl,g.Title as Title,c.Id,c.GoodsId as goodsid
	Into #temp
	From T_WxAppBannerConfig c
	Left Join T_Goods g on c.GoodsId = g.Id
	declare @records int
	
	exec P_ToPage @TableName ='#temp',@searchField='Title,ImgUrl',@searchString=@filter,@searchOper='in',@PageSize=@pageSize,@PageIndex=@pageIndex,
		@PageTotal=@records output,@RecordTotal=@total output, @OrderByField='Title',@OrderByType=''
End
Go
