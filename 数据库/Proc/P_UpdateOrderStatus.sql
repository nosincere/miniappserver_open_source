If OBJECT_ID('P_UpdateOrderStatus') Is Not Null
	Drop Procedure P_UpdateOrderStatus
Go

Create Procedure P_UpdateOrderStatus
(
	@orderid		int,
	@status			int,	--10待付款，20待发货，30已发货，40待评价，50已评价，-1取消（删除）订单
	@expressno		nvarchar(50),
	@expresscompany	nvarchar(50)
)
As
Begin
	If Exists(Select 1 From T_Order where Id=@orderid)
	Begin
		Declare @orderstatustext nvarchar(10),@paystatus int,@paystatustext nvarchar(10),
			@deliverystatus int,@deliverystatustext nvarchar(10),@receiptstatus int,@receiptstatustext nvarchar(10),
			@commentstatus int,@commentstatustext nvarchar(10),@userid int
			Select @commentstatustext = '',@commentstatus=0
		If @status = 10
		Begin
			Select @orderstatustext = '待付款',
				@paystatus=10,@paystatustext='待付款',
				@deliverystatus=0,@deliverystatustext='',
				@receiptstatus = 0,@receiptstatustext=''
		End
		Else If @status = 20
		Begin
			Select @orderstatustext = '待发货',
				@paystatus=20,@paystatustext='已付款',
				@deliverystatus=10,@deliverystatustext='待发货',
				@receiptstatus = 0,@receiptstatustext=''
			
			Update T_Order set CheckPayResult = 2 where Id = @orderid
				
			--增加用户积分
			Select @userid = UserId From T_Order where Id=@orderid
			
			select OrderId,Min(Id) as OrderDetailId Into #temporder From T_OrderDetail Where OrderId = @orderid Group By OrderId
		
			Insert Into T_WxMiniAppUserPoiontsLog(UserId,[Type],Points,SignDate,RelationId,Comment)
			select o.UserId,'order',o.TotalAmount,CONVERT(varchar(100), GETDATE(), 108),@orderid,'订单【'+ o.OrderNo +'】，'+ case when o.TotalNumber > 1 then g.Title + ' 等'+cast(o.TotalNumber AS nvarchar) + '商品' else g.Title end as Title
			from T_Order o 
			inner join #temporder od on o.Id = od.OrderId
			Inner Join T_OrderDetail d on od.OrderDetailId = d.Id
			Inner Join T_Goods g on d.GoodsId = g.Id
			Where o.Id = @orderid
			
			Drop Table #temporder
		End
		Else If @status = 30
		Begin
			Select @orderstatustext = '待收货',
				@paystatus=20,@paystatustext='已付款',
				@deliverystatus=20,@deliverystatustext='已发货',
				@receiptstatus = 10,@receiptstatustext='待收货'
			Update T_OrderStatus Set ExpressNo = @expressno,ExpressCompany=@expresscompany
			Where OrderId = @orderid
		End
		Else If @status = 40
		Begin
			Select @orderstatustext = '已收货',
				@paystatus=20,@paystatustext='已付款',
				@deliverystatus=20,@deliverystatustext='已发货',
				@receiptstatus = 20,@receiptstatustext='已收货',
				@commentstatus = 10,@commentstatustext='待评价'
		End
		Else If @status = 50
		Begin
			Select @orderstatustext = '已完成',
				@paystatus=20,@paystatustext='已付款',
				@deliverystatus=20,@deliverystatustext='已发货',
				@receiptstatus = 20,@receiptstatustext='已收货',
				@commentstatus = 20,@commentstatustext='已评价'
		End
		
		If @status = -1
		Begin
			Delete From T_Delivery where OrderId = @orderid
			Delete From T_Order Where Id = @orderid
			Delete From T_OrderDetail Where OrderId = @orderid
			DElete From T_OrderStatus where OrderId = @orderid
		End
		Else
		Begin
			Update T_OrderStatus Set OrderStatus=@status,OrderStatusText=@orderstatustext,
				PayStatus=@paystatus,PayStatusText=@paystatustext,
				DeliveryStatus=@deliverystatus,DeliveryStatusText=@deliverystatustext,
				ReceiptStatus=@receiptstatus,ReceiptStatusText=@receiptstatustext,
				CommentStatus = @commentstatus,CommentStatusText = @commentstatustext
				Where OrderId = @orderid
			Update T_Order set OrderStatus = @status,OrderStatusText=@orderstatustext where Id=@orderid
		End
	End
End
Go


