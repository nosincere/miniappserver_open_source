--首页公告
If Not Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[T_Messages]'))                        
CREATE TABLE [dbo].[T_Messages]
(
   [Id]		int IDENTITY(1,1) NOT NULL,	--自增长字段
   [cTitle]		[nvarchar](100)	 NOT NULL  DEFAULT(''),		--公告名称
   [cPicPath]	[nvarchar](256)	 NOT NULL  DEFAULT(''),		--图片路径
   [cAuthor]    [nvarchar](100)	 NOT NULL  DEFAULT(''),		--发布人
   [cDescription] [nvarchar](max)	 NOT NULL  DEFAULT(''),	--图文详情
   [dAddTime]		[datetime] NOT NULL,					--添加时间  
   [dPublishTime]	[datetime],								--发布时间
   [iOrder] [int] NOT NULL  DEFAULT(0),						--排序权重
   [iState] [int] NOT NULL  DEFAULT(0)						--发布状态
)
GO  

IF OBJECT_ID('P_GetMessage') IS NOT NULL
	Drop Procedure P_GetMessage
Go
Create Procedure P_GetMessage
(
	@Count		int = 3
)
As
Begin
	Select * from(
		Select *,ROW_NUMBER() over(order by iorder desc) as [order] 
		from T_Messages 
		--order by iOrder desc
		) T
	where t.[order] <= @count
End
Go

IF OBJECT_ID('P_AddMessage') IS NOT NULL
	Drop Procedure P_AddMessage
Go
Create Procedure P_AddMessage
(
	@title			nvarchar(200),
	@picpath		nvarchar(200),
	@author			nvarchar(200),
	@description	nvarchar(max),
	@order			int
)
As
Begin
	insert into T_Messages(cTitle,cPicPath,cAuthor,cDescription,iOrder,dAddTime)
	select @title,@picpath,@author,@description,@order,GETDATE()
End
Go

IF OBJECT_ID('P_ModifyMessage') IS NOT NULL
	Drop Procedure P_ModifyMessage
Go
Create Procedure P_ModifyMessage
(
	@id				int,
	@title			nvarchar(200),
	@picpath		nvarchar(200),
	@author			nvarchar(200),
	@description	nvarchar(max),
	@order			int
)
As
Begin
	Create Table #ErrMsg(Code int,Msg nvarchar(200))
	if not exists(select 1 from T_Messages where Id=@id)
	begin
		insert into #ErrMsg(Code,Msg)
		select -1,'公告不存在'
	end
	else
	begin
		update T_Messages set cTitle=@title,cPicPath=@picpath,cAuthor=@author,cDescription=@description,iOrder=@order
		where Id=@id	
	end
	select * from #ErrMsg
End
Go

IF OBJECT_ID('P_DeleteMessage') IS NOT NULL
	Drop Procedure P_DeleteMessage
Go
Create Procedure P_DeleteMessage
(
	@id	int
)
As
Begin
	Create Table #ErrMsg(Code int,Msg nvarchar(200))
	if not exists(select 1 from T_Messages where Id=@id)
	begin
		insert into #ErrMsg(Code,Msg)
		select -1,'公告不存在'
	end
	else
	begin
		delete from T_Messages 
		where Id=@id	
	end
	select * from #ErrMsg
End
Go