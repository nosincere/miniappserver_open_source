IF OBJECT_ID('P_GetCategoryListTable') IS Not Null
	Drop Procedure P_GetCategoryListTable
Go
Create procedure P_GetCategoryListTable
(
	@pageSize	int,
	@pageIndex	int,
	@sort		nvarchar(500),
	@order		nvarchar(500),
	@filter		nvarchar(500),--eq等于、ne不等于、lt小于、le小于等于、gt大于、ge大于等于、nu不存在 searchString为空、nn存在	searchString、in属于、ni不属于
	@total		int output
)
As
Begin
	Declare @configleveal Int
	Select @configleveal = SubValue From T_WxAppConfig Where SubKey = 'CategoryLeveal'
	;with cte 
	as (
		select id,Cast(name as nvarchar(max)) as Name,ImgUrl,
			Cast('' As nvarchar(Max)) _Name_class,
			orderindex,ParId,GoodsIds,ShowInIndexPage,case ShowInIndexPage when 1 then '是' else '否' end as ShowInIndexPageStr, Leveal, CAST(OrderIndex As nvarchar(Max)) As RowNum
		from T_Category 
		where parid = 0
		union all
		select c.id,Cast(c.name As nvarchar(max)),c.ImgUrl,
			Cast(Case When c.Leveal = @configleveal Then 'subnameright' When c.Leveal < @configleveal Then 'subnamecenter' Else '' End As nvarchar(Max)) _Name_class,
			case c.orderindex when 0 then 1 else c.OrderIndex end as orderindex,c.ParId,c.GoodsIds,c.ShowInIndexPage,case c.ShowInIndexPage when 1 then '是' else '否' end as ShowInIndexPageStr, c.Leveal,
			Cast(a.RowNum + '-' + CAST(c.OrderIndex As nvarchar) as nvarchar(max)) As RowNum
		from T_Category c
		inner join cte a on c.parid = a.id
	)
	
	Select Id,Name,ImgUrl,RowNum,_Name_class,OrderIndex,ParId,GoodsIds,ShowInIndexPage,ShowInIndexPageStr, Leveal
	into #temp
	from cte 
	order by  RowNum

	
	Delete From #temp Where Leveal > @configleveal
	select * from #temp Order By RowNum
	select @total = COUNT(*) from #temp
	--declare @records int
	Drop Table #temp
	--exec P_ToPage @TableName ='#temp',@searchField='Name,ImgUrl',@searchString=@filter,@searchOper='in',@PageSize=@pageSize,@PageIndex=@pageIndex,
	--	@PageTotal=@records output,@RecordTotal=@total output, @OrderByField='',@OrderByType=''
End
Go