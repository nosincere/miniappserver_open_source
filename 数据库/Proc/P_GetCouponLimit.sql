If OBJECT_ID('P_GetCouponLimit') Is Not Null
	Drop Procedure P_GetCouponLimit
Go
Create Procedure P_GetCouponLimit
(
	@id	int,
	@table nvarchar(20)
)
As
Begin
	declare @text nvarchar(max)
	select @text=''
	select distinct cl.CouponId clId,ISNULL(g.Id,a.Id) gId,ISNULL(g.Title,a.Title) gTitle ,g.iOrder
	into #t
	from T_CouponLimit cl 
	Left join T_Goods g on cl.GoodsId = g.Id 
	Left Join (select 0 as Id,'全部' as Title) a on cl.GoodsId = a.Id
	where cl.CouponId = @id 
	group by cl.CouponId,g.Id,g.Title ,g.iOrder,a.Title,a.Id
	order by g.iOrder
	
	DECLARE myCursor Cursor
	FOR (select clId,gId,gTitle from #t)
	OPEN myCursor --当全局游标和局部游标变量重名时，默认打开局部游标 
	DECLARE @clId int, @gId int,@gTitle nvarchar(400)
	FETCH NEXT FROM myCursor INTO @clId,@gId,@gTitle --如果需要

	WHILE @@FETCH_STATUS=0--和全局变量@@FETCH_STATUS与WHILE 

	BEGIN
		declare @detailstr nvarchar(max)
		select @detailstr = ''
		select @detailstr = case when len(@detailstr) = 0 then '' else @detailstr end + '{"sku":"' + ISNULL(gd.Name,a.Name) + '"},'
		From T_CouponLimit cl
		Left Join T_GoodsDetail gd on cl.SkuId = gd.Id
		Left Join (select 0 as Id,'全部' as Name) a on cl.SkuId = a.Id
		where cl.CouponId = @id
			And cl.GoodsId = @gId
		order by gd.iOrder
		
		if LEN(@detailstr) > 0
		Select @detailstr = LEFT(@detailstr,len(@detailstr) - 1)
		
		select @text = case when len(@text) = 0 then '' else @text end + '{"gname":"' + @gTitle + '","skus":' + '[' + @detailstr + ']},'
		
		FETCH NEXT FROM myCursor INTO @clId,@gId,@gTitle --如果需要
	END

	--4关闭游标
	CLOSE myCursor

	--5释放游标
	DEALLOCATE myCursor
	if LEN(@text) > 0
		Select @text = LEFT(@text,len(@text) - 1)
	select @text = '['+@text+']'
	if ISNULL(@table,'')<>''
	begin
		declare @sql nvarchar(max)
		select @sql ='insert into '+@table+' (result) select @text result'
		exec sp_executesql  @sql,N'@text nvarchar(max)',
			@text=@text
	end
	else
		select @text Result
	print @text
	drop table #t
End
Go