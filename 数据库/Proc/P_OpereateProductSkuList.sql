IF OBJECT_ID('P_OpereateProductSkuList') IS Not Null
	Drop Procedure P_OpereateProductSkuList
Go
Create procedure P_OpereateProductSkuList
(
	@Id			int,
	@goodsid	int,
	@Name		nvarchar(50),
	@costprice	numeric(18,2),
	@price		numeric(18,2),
	@kucun		nvarchar(100),
	@imgurl		nvarchar(200),
	@iorder		int,
	@bdisplay	bit
)
As
Begin
	If ISNULL(@Id,0) = 0
	begin
		Insert Into T_GoodsDetail(Name,costprice,Price,KuCun,ImgUrl,bDisplay,GoodsId,iOrder)
		select @Name,@costprice,@price,@kucun,@imgurl,@bdisplay,@goodsid,@iorder
	end
	else if ISNULL(@Id,0)<>0
	begin
		update T_GoodsDetail set Name = @Name,costprice=@costprice,Price=@price,KuCun=@kucun,ImgUrl=@imgurl,iOrder=@iorder,bDisplay=@bdisplay
		where Id=@Id
	end
	
End
Go
