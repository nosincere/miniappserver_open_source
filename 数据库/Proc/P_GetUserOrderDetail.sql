IF OBJECT_ID('P_GetUserOrderDetail') IS Not Null
	Drop Procedure P_GetUserOrderDetail
Go
Create procedure P_GetUserOrderDetail
(
	@orderid	int
)
As
Begin
	--@orderstatus 存放了商品，因为一个订单可能两个包裹
	--@orderoperate 底部按钮
	Declare @ResultData nvarchar(max),@address nvarchar(max),@goods nvarchar(max),@orderstatus Nvarchar(max),
		@order nvarchar(max),@orderoperate nvarchar(max)
	
	select @address = ''
	Select @address = '{'+
		'"name":"'+cPerson+'",'+
		'"phone":"'+cTelephone+'",'+
		'"province":"'+cProvince+'",'+
		'"city":"'+cCity+'",'+
		'"county":"'+cCounty+'",'+
		'"detail":"'+cAddress+'"'+
	'}'
	From T_Delivery where orderid = @orderid
	
	Select @address = ISNULL(@address,'')
	
	Create Table #good
	(
		ExpressNo	nvarchar(100),
		Detail		nvarchar(max)
	)
	
	
	DECLARE myCursor Cursor
	FOR (Select ExpressNo from T_OrderStatus where OrderId = @orderid group by ExpressNo)
	OPEN myCursor --当全局游标和局部游标变量重名时，默认打开局部游标 
	DECLARE @ExpressNo nvarchar(100)
	FETCH NEXT FROM myCursor INTO @ExpressNo

	WHILE @@FETCH_STATUS=0--和全局变量@@FETCH_STATUS与WHILE 

	BEGIN
		Select @goods = ''
		Select @goods = @goods +'{' +
				'"goods_id":' + CAST(g.Id as Nvarchar) +','+
				'"sku_id":' + CAST(od.GoodsSkuId as Nvarchar) +','+
				'"orderdetailid":' + CAST(od.Id as Nvarchar) +','+
				'"imgurl":"' + gd.ImgUrl +'",'+
				'"goods_name":"' + g.Title +'",'+
				'"goods_attr":"' + gd.Name +'",'+
				'"total_num":'+CAST(od.Number as Nvarchar)+','+
				'"goods_price":'+Cast(od.Price as nvarchar)+','+
				'"deliverystatus":'+Cast(os.DeliveryStatus as nvarchar)+','+
				'"commentstatus":'+CAST(os.CommentStatus as nvarchar)+','+
				'"commentstatustext":"'+os.CommentStatusText+'"'+
			'},'
		from T_OrderStatus os 
		Inner Join T_OrderDetail od on os.OrderDetailId = od.Id
		Inner Join T_GoodsDetail gd on od.GoodsSkuId = gd.Id
		Inner Join T_Goods g on od.GoodsId = g.Id
		Where ISNULL(ExpressNo,'') = ISNULL(@ExpressNo,'') And os.OrderId = @orderid
		
		If LEN(@goods) > 0
			Select @goods = LEFT(@goods,LEN(@goods)-1)
		select @goods ='[' + ISNULL(@goods,'') + ']'
			
		Insert Into #good(ExpressNo,Detail)
		Select ISNULL(@ExpressNo,''),@goods
		
		FETCH NEXT FROM myCursor INTO @ExpressNo
	END
	CLOSE myCursor
	DEALLOCATE myCursor

	
	Select @orderstatus=''
	Select @orderstatus =@orderstatus + '{'+
		'"orderstatus":'+CAST(OrderStatus as nvarchar)+','+
		'"orderstatustext":"'+OrderStatusText+'",'+
		'"deliverystatus":'+CAST(DeliveryStatus as nvarchar)+','+
		'"deliverystatustext":"'+DeliveryStatusText+'",'+
		'"expresscompany":"'+ISNULL(kd.Name,'')+'",'+
		'"expresscompanycode":"'+ISNULL(kd.Code,'')+'",'+
		'"expressno":"'+ISNULL(os.ExpressNo,'')+'",'+
		'"number":"'+cast(os.Number as nvarchar)+'",'+
		'"total":"'+cast(os.Total as nvarchar)+'",'+
		'"goods":'+d.Detail+''+
	'},'
	From
	(
		Select o.OrderId,DeliveryStatus,DeliveryStatusText,ExpressNo,ExpressCompany,min(OrderStatus) as OrderStatus,MIN(OrderStatusText) OrderStatusText,
			SUM(d.Number) as Number,SUM(d.Price * d.Number) as Total
		from T_OrderStatus o
		inner join T_OrderDetail d on o.OrderDetailId = d.Id
		Where o.OrderId = @orderid
		group by o.OrderId,DeliveryStatus,DeliveryStatusText,ExpressNo,ExpressCompany
	) os
	Left Join #good d on ISNULL(os.ExpressNo,'') = ISNULL(d.ExpressNo,'')
	left Join T_KdConfig kd on ISNULL(os.ExpressCompany,'') = kd.Code
	
	
	If LEN(@orderstatus) > 0
		Select @orderstatus = LEFT(@orderstatus,LEN(@orderstatus)-1)
	select @orderstatus ='[' + ISNULL(@orderstatus,'') + ']'
	
	
	Select @order = ''
	--Select @goods = ''
	--Select @goods=@goods+'{'+
	--		'"goods_id":'+CAST(d.GoodsId as Nvarchar)+','+
	--		'"imgurl":"'+gd.ImgUrl+'",'+
	--		'"goods_name":"'+g.Title+'",'+
	--		'"goods_attr":"'+gd.Name+'",'+
	--		'"total_num":'+CAST(d.Number as Nvarchar)+','+
	--		'"goods_price":'+Cast(d.Price as nvarchar)+','+
	--		'"deliverystatus":'+Cast(os.DeliveryStatus as nvarchar)+''+
	--	'},'
	--	From T_OrderDetail d
	--	Inner Join T_GoodsDetail gd on d.GoodsSkuId = gd.Id
	--	Inner Join T_Goods g on d.GoodsId = g.Id
	--	Inner Join T_OrderStatus os on d.Id = os.OrderDetailId
	--	Where d.OrderId = @orderid
	--Select @goods = ISNULL(@goods,'')
	--IF LEN(@goods)>0
	--	Select @goods = LEFT(@goods,LEN(@goods)-1)
	select @goods=''
	Select @order = '{"goods":['+@goods+'],"total_price":'+CAST((TotalAmount - DeliveryAmount + CouponAmount) as Nvarchar)+','+
			'"express_price":'+CAST(DeliveryAmount as nvarchar)+','+
			'"coupon_price":'+CAST(CouponAmount as nvarchar)+','+
			'"pay_price":'+CAST(TotalAmount as nvarchar)+','+
			'"order_no":'+CAST(OrderNo as nvarchar)+','+
			'"comment":"'+ comment +'",'+
			'"create_time":"'+CONVERT(varchar(100), dAddTime, 23) + ' ' + CONVERT(varchar(100), dAddTime, 108)+'"'+
		'}' 
		From T_Order where Id=@orderid
	
	Select @orderoperate = ''

	If Exists(Select 1 From T_OrderStatus where OrderId = @orderid and PayStatus <> 10)
		Select @orderoperate = '"paystatus":20,'
	Else
		Select @orderoperate = '"paystatus":10,'

	If Exists(Select 1 From T_OrderStatus where OrderId = @orderid and (DeliveryStatus = 10 or ReceiptStatus = 20))
		Select @orderoperate = @orderoperate + '"deliverystatus":10,"receiptstatus":0'
	Else
		Select @orderoperate = @orderoperate + '"deliverystatus":20,"receiptstatus":10'

	Select @orderoperate = '{'+@orderoperate+'}'

	Select @ResultData = '{"address":'+@address+',"order_id":'+Cast(@orderid as nvarchar)+',"orderstatus":'+@orderstatus+','+
			'"order":'+@order+','+
			'"orderoperate":'+ @orderoperate +
		'}' 
	Select @ResultData Result
	
End
Go

--exec P_GetUserOrderDetail 9