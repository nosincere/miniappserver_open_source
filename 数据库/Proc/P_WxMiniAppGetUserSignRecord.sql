If OBJECT_ID('P_WxMiniAppGetUserSignRecord') Is Not Null
	Drop Procedure P_WxMiniAppGetUserSignRecord
Go
--用户签到，签到时间以服务器时间为准
Create Procedure P_WxMiniAppGetUserSignRecord
(
	@token	nvarchar(50),
	@datestr nvarchar(10)
)
As
Begin
	Select @token = dbo.GetUserId(@token)
	Declare @continueday int,@result nvarchar(max),@moredays int
	Select @continueday  = dbo.GetUserContinueSignDay(@token)
	If @continueday < 7
		Select @moredays = 7 - @continueday
	Else
		Select @moredays = 0
	Select @result = ''
	Select @result = @result +
		CAST(CAST(Right(SignDate,2) as int) as nvarchar) +','
	From T_WxMiniAppUserPoiontsLog 
	Where [Type]='sign'
		And UserId = @token
		And Left(SignDate,7) = Left(@datestr,7)
	Order By SignDate
	If LEN(@result)	> 0
		Select @result = LEFT(@result,LEN(@result) - 1)
	Select @result = ISNULL(@result,'')	
	Select @result = '{"continuedays":'+CAST(@continueday as nvarchar)+',"moredays":'+CAST(@moredays as nvarchar)+',"record":['+@result+']}'
	Select @result Result
End
Go
