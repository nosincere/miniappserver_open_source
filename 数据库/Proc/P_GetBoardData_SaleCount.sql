If OBJECT_ID('P_GetBoardData_SaleCount') Is Not Null
	Drop Procedure P_GetBoardData_SaleCount
Go

Create Procedure P_GetBoardData_SaleCount
(
	@day	int
)
As
Begin
	Declare @date datetime
	select @day = ISNULL(@day,30)
	select @date = dateadd(day,-(@day-1),CONVERT(varchar(100), GETDATE(), 23)) 
	
	--legend
	Create Table #legend
	(
		GoodsId		int,
		GoodsIdStr	nvarchar(20),
		Name		Nvarchar(20),
		TotalName	nvarchar(200),
		Total		numeric(10,2),
		iOrder		int--排序，需要将“其他”放在最后
	)
	
	Insert Into #legend(GoodsId,GoodsIdStr,Name,TotalName,Total,iOrder)
	Select top 3 g.Id,'Goods' +CAST(g.Id as nvarchar), case when Len(Title) > 10 Then LEFT(Title,10) else Title end,Title,d.Number,1
	From T_Goods g
	Inner Join(
				Select GoodsId,SUM(Number) AS Number,SUM(Price * Number) AS Total 
				from T_OrderDetail 
				where dAddTime >= @date
				Group By GoodsId
		) d on g.Id = d.GoodsId
	Order By d.Number desc
	
	Insert Into #legend(GoodsId,GoodsIdStr,Name,TotalName,Total,iOrder)
	Select 0,'Goods0','其他','其他',ISNULL(SUM(Number),0),0
	from T_OrderDetail 
	where dAddTime >= @date
	And GoodsId not in(select GoodsId from #legend)
	Order By SUM(Price * Number) desc
	
	If Not exists(select 1 from #legend where GoodsId=0)
		Insert Into #legend(GoodsId,GoodsIdStr,Name,TotalName,Total,iOrder)
		Select 0,'Goods0','其他','其他',0,0
	
	--x坐标
	Create Table #xAxis
	(
		Name	nvarchar(10),
		DName	nvarchar(11)
	)
	--Insert Into #xAxis(Name,DName)
	--Select CONVERT(varchar(100),@date , 23),CONVERT(varchar(100), @date, 112) Union All
	--Select CONVERT(varchar(100),dateadd(day,1,@date) , 23),CONVERT(varchar(100),dateadd(day,1,@date) , 112) Union All
	--Select CONVERT(varchar(100),dateadd(day,2,@date) , 23),CONVERT(varchar(100),dateadd(day,2,@date) , 112) Union All
	--Select CONVERT(varchar(100),dateadd(day,3,@date) , 23),CONVERT(varchar(100),dateadd(day,3,@date) , 112) Union All
	--Select CONVERT(varchar(100),dateadd(day,4,@date) , 23),CONVERT(varchar(100),dateadd(day,4,@date) , 112) Union All
	--Select CONVERT(varchar(100),dateadd(day,5,@date) , 23),CONVERT(varchar(100),dateadd(day,5,@date) , 112) Union All
	--Select CONVERT(varchar(100),dateadd(day,6,@date) , 23),CONVERT(varchar(100),dateadd(day,6,@date) , 112)
	
	declare @loopdate datetime
	select @loopdate = @date
	while @day > 0
	begin
		Insert Into #xAxis(Name,DName)
		Select CONVERT(varchar(100),@loopdate , 23),CONVERT(varchar(100), @loopdate, 112)
		select @loopdate = dateadd(day,1,CONVERT(varchar(100), @loopdate, 23)) 
		select @day = @day - 1
	end
	
	Create Table #series
	(
		Id			nvarchar(20),
		[Date]		nvarchar(11),
		Title		nvarchar(10),
		TotalTitle	nvarchar(200),
		Total		numeric(18,2)
	)
	Insert Into #series(Id,[Date], Title,TotalTitle,Total)
	Select 'Goods'+CAST(Id as nvarchar),'D' + d.[Date], case when Len(Title) > 10 Then LEFT(Title,10) else Title end as Title,Title as TotalTile,d.Number
	From T_Goods g
	Inner Join(
				Select GoodsId,SUM(Number) AS Number,SUM(Price * Number) AS Total ,CONVERT(varchar(100),dAddTime , 112) as [Date]
				from T_OrderDetail 
				where dAddTime >= @date
				Group By GoodsId, CONVERT(varchar(100),dAddTime , 112)
		) d on g.Id = d.GoodsId
	Inner Join #legend l on g.Id = l.GoodsId
	Order By d.Number desc
	
	Insert Into #series(Id,[Date],Title,TotalTitle,Total)
	Select 'Goods0','D'+CONVERT(varchar(100),dAddTime , 112) as [Date],'其他' as Title,'其他' as TotalTitle,ISNULL(SUM(Number),0) AS Total
	from T_OrderDetail 
	where dAddTime >= @date
	And GoodsId not in(select GoodsId from #legend)
	Group By CONVERT(varchar(100),dAddTime , 112)
	Order By SUM(Number) desc
	
	IF Not Exists(Select 1 from #series where Id='Goods0')
		Insert Into #series(Id,[Date],Title,TotalTitle,Total)
		Select 'Goods0','','其他','其他',0
	

	--select * from #series return
	declare @sql nvarchar(max)
	update #xAxis set DName = 'D' + DName
	Select @sql=''
	select @sql = @sql + '[' +DName+'],'
		From #xAxis
	if LEN(@sql) > 0
		Select @sql = LEFT(@sql,LEN(@sql) - 1)

	Select @sql = 'select * into #tempr2 from #series r pivot(Max(Total) for [Date] in(' + @sql + ')) t'
	Select @sql = @sql +' select t2.* from #legend l left join #tempr2 t2 on l.GoodsIdStr = t2.Id order by l.iorder desc, l.Total desc' 
	print @sql
	
	Select * from #legend 
	Select * from #xAxis 
	--Select * from #series
	EXEC sp_executesql @SQL
	
	Drop Table #legend
	Drop table #xAxis
	Drop table #series
End
Go
