--确认收货
IF OBJECT_ID('P_UserOrderReceipt') IS Not Null
	Drop Procedure P_UserOrderReceipt
Go
Create procedure P_UserOrderReceipt
(
	@token		nvarchar(50),
	@orderid	int
)
As
Begin
	Select @token = dbo.GetUserId(@token)
	if not exists(select 1 from T_Order o 
			inner join T_WxUser u on o.UserId = u.[user_id]
			where u.[user_id] = @token
				And o.Id = @orderid)
	Begin
		Select '{"retcode":-500,"retmsg":"不能操作其他用户订单"}' Result
		return
	End
	
	Update T_Order set OrderStatus = 40,OrderStatusText='待评价' where Id = @orderid
	Update T_OrderStatus set OrderStatus = 40,OrderStatusText='待评价',ReceiptStatus=20,ReceiptStatusText='已收货',CommentStatus=10,CommentStatusText='待评价' where OrderId = @orderid

End
Go
