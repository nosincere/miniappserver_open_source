If OBJECT_ID('P_GetCouponLimitWx') Is Not Null
	Drop Procedure P_GetCouponLimitWx
Go

Create Procedure P_GetCouponLimitWx
(
	@cid	int
)
As
Begin
	declare @result nvarchar(max)
	create table #temp(result nvarchar(max))
	exec P_GetCouponLimit @cid,'#temp'	
	select @result = result from #temp
	select @result = '{"limit":' + ISNULL(@result,'[]') + '}'
	select @result Result
	drop table #temp
End
Go

