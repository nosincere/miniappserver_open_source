If OBJECT_ID('P_KuCunDetailReport') Is Not Null
	Drop Procedure P_KuCunDetailReport
Go
Create Procedure P_KuCunDetailReport
(
	@pageSize	int,
	@pageIndex	int,
	@sort		nvarchar(500),
	@order		nvarchar(500),
	@filter		nvarchar(500),--eq等于、ne不等于、lt小于、le小于等于、gt大于、ge大于等于、nu不存在 searchString为空、nn存在	searchString、in属于、ni不属于
	@DateStart	nvarchar(10),
	@DeteEnd	nvarchar(10),
	@GoodsId	int,
	@totalcount	int output
)
AS
Begin
	set nocount on
	Create Table #Result
	(
		Id			int identity(1,1),
		SortColumn	int,
		SkuId		int,
		GoodsId		int,
		GoodsName	nvarchar(1000),
		SkuName		nvarchar(500),
		OrderNo		nvarchar(200),
		dAddTime	nvarchar(20),
		Total		int,
		Balance		int,
		OrderId		int
	)
	
	Insert Into #Result(SkuId,GoodsId,GoodsName,SkuName,OrderNo,dAddTime ,Total,Balance,OrderId)
	Select gd.Id,g.Id,g.Title,gd.Name,ISNULL(od.OrderNo,''),CONVERT(varchar(100), od.odAddTime, 23) +' ' + CONVERT(varchar(100), od.odAddTime, 108),
		ISNULL(od.Number,0),0 ,ISNULL(od.Id,0)
	From T_GoodsDetail gd 
	Inner Join T_Goods g on gd.GoodsId = g.Id 
	Left Join(Select od.GoodsSkuId,od.dAddTime,o.OrderNo,o.dAddTime odAddTime,od.Number,o.Id From T_OrderDetail od 
				Inner Join T_Order o on od.OrderId = o.Id And o.OrderStatus <> -10) od on od.GoodsSkuId = gd.Id And od.dAddTime between @DateStart and @DeteEnd 
	Where (g.Id = @GoodsId Or @GoodsId=0)
		
	Order By g.Id,od.GoodsSkuId asc,od.dAddTime asc

	DECLARE myCursor Cursor
	FOR select Id,GoodsId,SkuId,Total from #Result order by Id
	OPEN myCursor 
	DECLARE @Id int, @SkuId int,@Total int,@lastSkuId	int,@SortColumn Int,@iniBalance int,@loopgoodsId int
	Select @SortColumn = 0,@lastSkuId = 0
	FETCH NEXT FROM myCursor INTO @Id,@loopgoodsId,@SkuId,@Total 

	WHILE @@FETCH_STATUS=0--和全局变量@@FETCH_STATUS与WHILE 
		
		BEGIN
			Select @SortColumn = @SortColumn + 1
			
			Update #Result Set SortColumn = @SortColumn where Id = @Id
			
			If @lastSkuId <> @SkuId
			Begin
				Select @iniBalance = KuCun + ISNULL(od.Number,0)
					From T_GoodsDetail gd
					Left Join (
						Select GoodsSkuId,SUM(Number) as Number From T_OrderDetail od Inner Join T_Order o on od.OrderId = o.Id where o.OrderStatus<>-10 Group By GoodsSkuId
					) od on gd.Id = od.GoodsSkuId
					where Id = @SkuId
			
				Select @SortColumn = @SortColumn + 1
				Insert Into #Result(SortColumn,SkuId,GoodsName,SkuName,OrderNo,Total,Balance,GoodsId)
				Select -1,@SkuId,'','','初始库存',0,@iniBalance,@loopgoodsId
				
				If @lastSkuId <> 0
				begin
					Insert Into #Result(SortColumn,SkuId,GoodsName,SkuName,OrderNo,Total,Balance,GoodsId)
					Select -2, @SkuId,'','','',null,null,@loopgoodsId
				end
				
			End
			
			Update #Result Set Balance = @iniBalance - Total where Id = @Id
			Set @iniBalance = @iniBalance - @Total
			Select @lastSkuId = @SkuId
			
			
			
			FETCH NEXT FROM myCursor INTO @Id,@loopgoodsId,@SkuId,@Total 
		END

	CLOSE myCursor

	DEALLOCATE myCursor
	
	--Select * from #Result Order By SkuId asc,SortColumn asc
	
	declare @records int
	exec P_ToPage @TableName ='#Result',@searchField='GoodsName,SkuName',@searchString=@filter,@searchOper='in',@PageSize=@pageSize,@PageIndex=@pageIndex,
		@PageTotal=@records output,@RecordTotal=@totalcount output, @OrderByField='GoodsId,SkuId,SortColumn',@OrderByType='asc'
End
Go


--exec P_KuCunDetailReport '2020-01-01','2020-12-31',0