If OBJECT_ID('P_GetIndexPageData') Is Not Null
	Drop Procedure P_GetIndexPageData
Go

Create Procedure P_GetIndexPageData
As
Begin
	--navbar
	Declare @ResultJosn NVARCHAR(MAX),@items nvarchar(max),@newest nvarchar(max),
		@searchStyle nvarchar(20),@searchtextAlign nvarchar(20),@searchplaceholder nvarchar(20),@bannerbtnColor nvarchar(20),@bannerbtnShape nvarchar(20),@bannerdata nvarchar(max),
		@best nvarchar(max),@shownew nvarchar(10),@categorylist nvarchar(max),@noticecontent nvarchar(200)
	Select @searchStyle			= SubValue	From T_WxAppConfig Where SubKey='searchStyle'
	Select @searchtextAlign		= SubValue	From T_WxAppConfig Where SubKey='searchtextAlign'
	Select @searchplaceholder	= SubValue	From T_WxAppConfig Where SubKey='searchplaceholder'
	Select @bannerbtnColor		= SubValue	From T_WxAppConfig Where SubKey='bannerbtnColor'
	Select @bannerbtnShape		= SubValue	From T_WxAppConfig Where SubKey='bannerbtnShape'
	Select @ResultJosn=''
	Select @bannerdata=''
	Select @bannerdata = @bannerdata +
	'{'+
		'"linkUrl":"'+linkUrl+'",'+
		'"imgUrl":"'+imgUrl+'"'+
	'},' From T_WxAppBannerConfig
	If LEN(@bannerdata) > 0
		Select @bannerdata = LEFT(@bannerdata,len(@bannerdata)-1)
	Select @items= '['+
			'{'+
				'"type":"search",'+
				'"searchStyle":"'+@searchStyle+'",'+
				'"textAlign":"'+@searchtextAlign+'",'+
				'"placeholder":"'+@searchplaceholder+'"'+

			'}'+
			',{'+
				'"type":"banner",'+
				'"btnColor":"'+@bannerbtnColor+'",'+
				'"btnShape":"'+@bannerbtnShape+'",'+
				'"data":['+@bannerdata
				+']'+
			'}'+
		']'
		
	--��Ʒ	
	Select @newest=''
	Select @newest = @newest +
		'{'+
			'"goods_id":'+Cast(g.Id as nvarchar)+','+
			'"goods_name":"'+g.Title+'",'+
			'"defaultimgurl":"'+ISNULL(d.ImgUrl,g.DefaultImgUrl)+'",'+
			'"hasprice":'+Cast(case when ISNULL(d.ImgUrl,'')='' then 0 else 1 end as nvarchar)+','+
			'"goods_price":"'+Cast(ISNULL(d.Price,0) as nvarchar)+'",'+
			'"goods_costprice":"'+Cast(ISNULL(d.costprice,0) as nvarchar)+'"'+
		'},' 
			
			from(
					select * from(
						Select Id,iOrder,Title,DefaultImgUrl,ROW_NUMBER() over(partition by bDisplay order by dDisplayTime desc) as [order] from T_Goods
						where bDisplay=1
						)t 
					where [order]<6) g
				Left Join (
					Select GoodsId,ImgUrl,price,costprice, ROW_NUMBER() over(partition by GoodsId order by Price asc) as [order] 
					from T_GoodsDetail
				) d on g.Id = d.GoodsId And d.[order] = 1
				order by g.[Order]
	If LEN(@newest) > 1
		Select @newest = LEFT(@newest,LEN(@newest)-1)
	Select @newest = '['+@newest+']'
	
	--����ϲ��
	Select @best=''
	Select @best = @best +
		'{'+
			'"goods_id":'+Cast(g.Id as nvarchar)+','+
			'"goods_name":"'+g.Title+'",'+
			'"defaultimgurl":"'+ISNULL(g.ImgUrl,g.DefaultImgUrl)+'",'+
			'"hasprice":'+Cast(case when ISNULL(g.ImgUrl,'')='' then 0 else 1 end as nvarchar)+','+
			'"goods_price":"'+Cast(ISNULL(g.Price,0) as nvarchar)+'",'+
			'"goods_costprice":"'+Cast(ISNULL(g.costprice,0) as nvarchar)+'"'+
		'},' 
			From(select top 999 g.Id,g.Title,d.ImgUrl,g.DefaultImgUrl,d.Price,d.costprice
			from T_Goods g
			left join (
				Select Id,ImgUrl,GoodsId,price,costprice from(
					Select Id,ImgUrl,GoodsId,price,costprice,ROW_NUMBER() over(partition by GoodsId order by iorder) as [order] 
					from T_GoodsDetail) t
				Where t.[order]=1
			) d on g.Id = d.GoodsId
			where g.bDisplay = 1
			order by g.iOrder,g.dDisplayTime
			)g
			--Where g.dDisplayTime > DATEADD(DD,-7,GETDATE())
	If LEN(@best) > 1
		Select @best = LEFT(@best,LEN(@best)-1)
	Select @best = '['+@best+']'
	
	select @shownew = SubValue from T_WxAppConfig where SubKey='WxHomePageShowNew'
	
	select @categorylist = ''
	
	
	select @categorylist = @categorylist +
		'{'+
			'"id":'+CAST(Id as nvarchar)+','+
			'"imgurl":"'+ImgUrl+'",'+
			'"name":"'+name+'"'+
		'},'
		from T_Category where ShowInIndexPage = 1
		
	If LEN(@categorylist) > 1
		Select @categorylist = LEFT(@categorylist,LEN(@categorylist)-1)
	Select @categorylist = '['+@categorylist+']'
	
	select @noticecontent = SubValue from T_WxAppConfig where SubKey='WxHomeNoticeContent'
	select @noticecontent = ISNULL(@noticecontent,'')
	
	Select @ResultJosn = '{"items":' + @items + ',"newest":' + @newest + ',"best":' + @best + ',"shownew":'+@shownew+',"categorylist":'+@categorylist+',"noticecontent":"'+@noticecontent+'"}'
	Select @ResultJosn 'Result'
End
Go

