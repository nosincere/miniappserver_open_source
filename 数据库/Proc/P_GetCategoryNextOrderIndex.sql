IF OBJECT_ID('P_GetCategoryNextOrderIndex') IS Not Null
	Drop Procedure P_GetCategoryNextOrderIndex
Go
Create procedure P_GetCategoryNextOrderIndex
(
	@parid	int
)
As
Begin
	declare @maxindex int,@count int
	If exists(select 1 from T_Category where ParId = @parid)
	Begin
		select @maxindex = ISNULL(MAX(OrderIndex),0) + 1 From T_Category where ParId = @parid
		select @count = ISNULL(count(OrderIndex),0) + 1 From T_Category where ParId = @parid
		if(@maxindex > @count)
			return @maxindex
		else
			return @count
	End
	Else
		return 1
End
Go