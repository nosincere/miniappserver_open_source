IF OBJECT_ID('P_SubmitOrderBuyNow') IS Not Null
	Drop Procedure P_SubmitOrderBuyNow
Go
Create procedure P_SubmitOrderBuyNow
(
	@token		nvarchar(50),
	@comment		nvarchar(400),
	@ucid	int,
	@gid		int,
	@number		int,
	@skuid		int,
	@retcode	int output,
	@msg		nvarchar(200) output,
	@orderid	int output
)
As
Begin
	Declare @ResultData nvarchar(max),@address nvarchar(max),@exist_address nvarchar(5),@goods_list nvarchar(max),
		@order_total_num int,@order_total_price numeric(18,2),@express_price numeric(18,2),@intra_region nvarchar(5),
		@order_pay_price numeric(18,2),
		@sn nvarchar(50),@totalamount numeric(18,2),@totalnumber int,@deliveryamount numeric(18,2),@errcount int,
		@coupontotal numeric(18,2)
	Select @token = dbo.GetUserId(@token)
	select @orderid = 0
	begin tran
	begin try  
		--检查库存
		If Exists(Select 1 From T_GoodsDetail d where d.KuCun < @number) 
			And Exists(Select 1 From T_WxAppConfig where SubKey='AllowNegativeKucun' and SubValue='0')
		Begin
			goto ErrorNoKuCun
			
		End
		Else
		Begin
			If Exists(Select 1 From T_GoodsDetail where Id=@skuid And GoodsId = @gid)
			Begin
				
				Select @sn = dbo.GetSn(round(100000*rand(),0))
				insert into T_Order (UserId,OrderNo,OrderStatus,OrderStatusText,Comment) values (@token,@sn,10,'待付款',@comment)
				Select @orderid = @@IDENTITY

				Insert Into T_OrderDetail(OrderId,GoodsId,GoodsSkuId,Price,Number)
				Select @orderid,@gid,@skuid, d.Price,@number
				from T_GoodsDetail d 
				Where d.Id = @skuid
				select @errcount = @@ROWCOUNT
				if @errcount = 0
					goto ErrorNoGoods
				
				Insert Into T_Delivery(OrderId,cPerson,cTelephone,cProvince,cCity,cCounty,cAddress,Total)
				Select @orderid,cPerson,cTelephone,a.cProvince,cCity,cCounty,cAddress,ISNULL(d.Price,d2.Price)
				From T_ShoppingAddress a
				Left Join T_DeliveryPrice d on a.cProvince = d.Province
				Left Join T_DeliveryPrice d2 on d2.Province='默认'
				Where a.bDefault = 1 and a.cUserId=@token
				select @errcount = @@ROWCOUNT
				if @errcount = 0
					goto ErrorNoAddress
				
				Select @deliveryamount = Total From T_Delivery Where OrderId = @orderid
				Select @totalamount = SUM(Price * Number) From T_OrderDetail Where OrderId = @orderid
				if ISNULL(@ucid,0)<>0
				begin
					select @coupontotal = Amount From T_Coupon c inner join T_UserCoupon uc on uc.CouponId = c.Id where uc.Id = @ucid
					update T_UserCoupon set IsUsed = 1 where Id=@ucid
				end
				select @coupontotal = ISNULL(@coupontotal,0)
				
				Select @totalamount = @totalamount + @deliveryamount - @coupontotal
				--select @totalamount,@deliveryamount,@coupontotal
				Select @totalnumber = SUM(Number) From T_OrderDetail Where OrderId = @orderid
				Update T_Order Set TotalAmount=@totalamount,TotalNumber = @totalnumber,DeliveryAmount = @deliveryamount  ,CouponAmount = @coupontotal,UserCouponId = @ucid
				where Id=@orderid

				Insert Into T_OrderStatus(OrderId,OrderDetailId,OrderStatus,OrderStatusText,PayStatus,PayStatusText)
				Select o.Id,d.Id,10,'待付款',10,'待付款' 
				From T_Order o
				Left Join T_OrderDetail d on o.Id = d.OrderId
				Where o.Id = @orderid

				--重新计算库存
				Update d Set d.KuCun = d.KuCun - o.Number
				From T_GoodsDetail d
				Inner Join T_OrderDetail o on d.Id = o.GoodsSkuId
				Where o.OrderId = @orderid
			End
			Else
			Begin
				Goto ErrorNoGoods
			End
		End
	end try
	begin catch
	   --select Error_number() as ErrorNumber,  --错误代码
			 -- Error_severity() as ErrorSeverity,  --错误严重级别，级别小于10 try catch 捕获不到
			 -- Error_state() as ErrorState ,  --错误状态码
			 -- Error_Procedure() as ErrorProcedure , --出现错误的存储过程或触发器的名称。
			 -- Error_line() as ErrorLine,  --发生错误的行号
			 -- Error_message() as ErrorMessage  --错误的具体信息
	   if(@@trancount>0) --全局变量@@trancount，事务开启此值+1，他用来判断是有开启事务
		  rollback tran  ---由于出错，这里回滚到开始，第一条语句也没有插入成功。
		Select @retcode = -20,@msg = Error_message()
		return
	end catch
	if(@@trancount>0)
	begin
		commit tran  --如果成功Lives表中，将会有3条数据。
		goto success		
	end
	
	ErrorNoKuCun:	
		Select @retcode = -20,@msg='库存不足'
		rollback tran
		return 
	ErrorNoGoods:
		Select @retcode = -20,@msg = '商品不存在'
		rollback tran
		return 
	ErrorNoAddress:
		Select @retcode = -20,@msg = '请添加地址'
		rollback tran
		return 
	success:
		Select @retcode = 0,@msg = '提交成功'
End
Go
