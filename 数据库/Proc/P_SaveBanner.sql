IF OBJECT_ID('P_SaveBanner') IS Not Null
	Drop Procedure P_SaveBanner
Go
Create procedure P_SaveBanner
(
	@Id			int,
	@ImgUrl		nvarchar(500),
	@Title		nvarchar(500),
	@msg		nvarchar(100) output
)
As
Begin
	select @msg = ''
	If ISNULL(@Id,0) <> 0
		Update T_WxAppBannerConfig set imgUrl = @ImgUrl,GoodsId = @Title,linkUrl = 'pages/goods/index?goods_id='+@Title Where Id = @Id
	Else
		Insert Into T_WxAppBannerConfig(linkUrl,ImgUrl,GoodsId)
		Select 'pages/goods/index?goods_id='+@Title,@ImgUrl,@Title
End
Go
