If OBJECT_ID('P_SetCartListChecked') Is Not Null
	Drop Procedure P_SetCartListChecked
Go

Create Procedure P_SetCartListChecked
(
	@token			nvarchar(200),
	@cartid			int
)
As
Begin
	Select @token = dbo.GetUserId(@token)
	if @cartid = 0
	begin
		if exists(select 1 from T_CartList where UserId = @token and checked = 0)
			update T_CartList set checked = 1 where UserId = @token
		else
			update T_CartList set checked = 0 where UserId = @token
	end
	else
		update T_CartList set checked = case when checked=1 then 0 else 1 end where id=@cartid and UserId=@token
		
	Declare @ResultData nvarchar(max)

	Select @ResultData = '{"orderCount":"�ɹ�"}'
	Select @ResultData Result
End
Go