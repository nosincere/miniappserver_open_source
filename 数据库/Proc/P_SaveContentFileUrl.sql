IF OBJECT_ID(N'P_SaveContentFileUrl') IS NOT NULL 
	DROP PROCEDURE P_SaveContentFileUrl
GO

CREATE PROCEDURE P_SaveContentFileUrl  
(
	@goodsid	int,
	@filexml	nvarchar(max)
)
As
Begin
	DECLARE @xmlHandle INT    
	EXEC sp_xml_preparedocument @xmlHandle OUTPUT, @filexml   
	SELECT  FileUrl
	INTO    #File
	FROM    OPENXML(@xmlHandle,N'/Root/Record')   
	WITH(FileUrl nvarchar(200))
	EXEC sp_xml_removedocument @xmlHandle

	If Exists(select 1 From T_GoodsContentFiles where GoodsId=@goodsid)
		Delete From T_GoodsContentFiles where GoodsId=@goodsid
	
	Insert Into T_GoodsContentFiles(GoodsId,ContentFileUrl)
	Select @goodsid,FileUrl
	From #File
End
Go