If OBJECT_ID('P_LoopOrder') Is Not Null
	Drop Procedure P_LoopOrder
Go
Create Procedure P_LoopOrder
As
Begin
	Select Id
	Into #CancelIds 
	from T_Order 
	where OrderStatus = 10 
		And DATEADD(mi,-30,GETDATE()) >= dAddTime
	
	Declare @id int	
	DECLARE Cur CURSOR LOCAL STATIC FORWARD_ONLY READ_ONLY
	FOR
	SELECT  Id
	FROM    #CancelIds
	OPEN Cur 
	FETCH NEXT FROM Cur INTO @id
	WHILE @@FETCH_STATUS = 0
	BEGIN
	  exec P_CancelOrder '',@id
	 
	  FETCH NEXT FROM Cur INTO @id
	END 
	CLOSE Cur
	DEALLOCATE Cur
End

Go