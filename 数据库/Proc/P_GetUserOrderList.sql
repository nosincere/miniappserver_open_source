IF OBJECT_ID('P_GetUserOrderList') IS Not Null
	Drop Procedure P_GetUserOrderList
Go
Create procedure P_GetUserOrderList
(
	@token		nvarchar(50),
	@dataType	nvarchar(20)
)
As
Begin
	Declare @ResultData nvarchar(max),@list nvarchar(max),@goods nvarchar(max),@orderid int
	Select @token = dbo.GetUserId(@token)
	
	Select *
	Into #Orders 
	From T_Order
	Where UserId=@token
		And ((@dataType = 'all') 
			Or (@dataType='payment' And OrderStatus=10)
			Or (@dataType='delivery' And OrderStatus=20)
			Or (@dataType='received' And OrderStatus=30)
			Or (@dataType='comment' And OrderStatus=40))
	Order By dAddTime desc
	Select @list = ''
	
	DECLARE myCursor Cursor
	FOR (select Id from #Orders where userid=@token)
	OPEN myCursor --当全局游标和局部游标变量重名时，默认打开局部游标 
	FETCH NEXT FROM myCursor INTO @orderid

	WHILE @@FETCH_STATUS=0--和全局变量@@FETCH_STATUS与WHILE 

	BEGIN
		select @goods = ''
		Select @goods = @goods + '{' +
				'"imgurl":"'+gd.ImgUrl+'"'+
			'},'
			From T_GoodsDetail gd
			Inner Join T_OrderDetail od on gd.Id = od.GoodsSkuId
			Inner Join T_Order o on od.OrderId = o.Id
			Where o.Id = @orderid
		Select @goods = ISNULL(@goods,'')
		If LEN(@goods) > 0
			Select @goods = LEFT(@goods,len(@goods)-1)
	
		Select @list = @list +'{'+
						'"order_id":'+Cast(o.Id as nvarchar)+','+
						'"pay_status":'+Cast(s.PayStatus as nvarchar)+','+
						'"pay_statustext":"'+s.PayStatusText+'",'+
						'"delivery_status":'+Cast(s.DeliveryStatus as nvarchar)+','+
						'"delivery_statustext":"'+s.DeliveryStatusText+'",'+
						'"receipt_status":'+CAST(s.ReceiptStatus as nvarchar)+','+
						'"receipt_statustext":"'+s.ReceiptStatusText+'",'+
						'"comment_status":'+CAST(s.CommentStatus as nvarchar)+','+
						'"comment_statustext":"'+s.CommentStatusText+'",'+
						'"order_status":'+CAST(s.OrderStatus as nvarchar)+','+
						'"order_statustext":"'+s.OrderStatusText+'",'+
						'"order_no":"'+o.OrderNo+'",'+
						'"create_time":"'+CONVERT(varchar(100), o.dAddTime, 23) + ' ' + CONVERT(varchar(100), o.dAddTime, 108) +'",'+
						'"totalnumber":'+CAST(o.TotalNumber as nvarchar)+','+
						'"pay_price":'+CAST(o.TotalAmount as nvarchar)+','+
						'"goods":['+@goods+']'+
					'},'
		from T_Order o
		Inner Join(
			Select * From(
				Select OrderId,Id,OrderStatus,OrderStatusText,PayStatus,PayStatusText,DeliveryStatus,DeliveryStatusText,ReceiptStatus,ReceiptStatusText,CommentStatus,CommentStatusText,
				ROW_NUMBER() over(PARTITION by orderId order by orderstatus asc) As OrderStatusIndex
				From T_OrderStatus
				) t Where t.OrderStatusIndex = 1
			) s On o.Id = s.OrderId
		where UserId = @token
			And o.Id = @orderid
	FETCH NEXT FROM myCursor INTO @orderid
	END

	CLOSE myCursor

	DEALLOCATE myCursor
	
	Select @list = ISNULL(@list,'')
	If LEN(@list) > 0
		Select @list = LEFT(@list,len(@list)-1)
	
	Select @ResultData = '{"list":['+@list+']}' 
	Select @ResultData Result
	
	Drop Table #Orders
End
Go
