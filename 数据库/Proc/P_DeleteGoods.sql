If OBJECT_ID('P_DeleteGoods') Is Not Null
	Drop Procedure P_DeleteGoods
Go
Create Procedure P_DeleteGoods
(
	@id		int,
	@msg	nvarchar(50) output
)
As
Begin
	Select @msg = ''
	If exists(select 1 from T_OrderDetail where GoodsId = @id)
	Begin
		Select @msg = '当前商品已有订单，不能删除，只能下架。'
	End
	Else
	Begin
		Delete From T_Goods where Id = @id
		Delete From T_GoodsDetail where GoodsId = @id
	End
End
Go