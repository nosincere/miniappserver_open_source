If OBJECT_ID('P_GetGoodsSkusSelect') IS Not null
	Drop Procedure P_GetGoodsSkusSelect
Go
Create Procedure P_GetGoodsSkusSelect
(
	@goodsid	int,
	@type nvarchar(20)
)
As
Begin
	Create Table #t(Id int,Title nvarchar(200))
	
	If ISNULL(@type,'')='includeall'
		Insert Into #t(Id,Title) select 0,'ȫ��'
	insert into #t(Id,Title)
	select Id,Name from T_GoodsDetail where bDisplay = 1 and GoodsId=@goodsid order by iOrder
	
	select * from #t
	
	Drop table #t
End
Go