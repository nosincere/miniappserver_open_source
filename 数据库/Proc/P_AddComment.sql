If OBJECT_ID('P_AddComment') Is Not Null
	Drop Procedure P_AddComment
Go
Create Procedure P_AddComment
(
	@token		nvarchar(50),
	@orderdetailid		int,
	@skuid		int,
	@star		int,
	@comment	nvarchar(2000)
)
As
Begin
	declare @goodsid int
	select @goodsid = GoodsId From T_GoodsDetail where Id = @skuid
	Select @token = dbo.GetUserId(@token)
	
	if not exists(select 1 from T_Order o 
			inner join T_OrderDetail d on o.Id = d.OrderId 
			inner join T_WxUser u on o.UserId = u.[user_id]
			where u.[user_id] = @token
				And d.Id = @orderdetailid)
	Begin
		Select '{"retcode":-500,"retmsg":"不能操作其他用户订单"}' Result
		return
	End
	
	if exists(select 1 from T_OrderDetailComment where GoodsId=@goodsid and GoodsSkuId=@skuid and OrderDetailId=@orderdetailid)
		Delete From T_OrderDetailComment where GoodsId=@goodsid and GoodsSkuId=@skuid and OrderDetailId=@orderdetailid
	
	insert into T_OrderDetailComment(GoodsId,GoodsSkuId,OrderDetailId,Stars,Comment,WxUserId)
	select @goodsid,@skuid,@orderdetailid,@star,@comment,@token
	
	Update T_OrderStatus set OrderStatus = 50,OrderStatusText='已完成',CommentStatus=20,CommentStatusText='已评价' where OrderDetailId = @orderdetailid
	
	declare @orderid int
	select @orderid = OrderId from T_OrderDetail where Id = @orderdetailid
	
	if not exists(select 1 from T_OrderStatus where OrderId = @orderid and OrderStatus<>50)
	begin
		--所有明细都评价完成，则修改订单状态为已完成
		update T_Order set OrderStatus = 50,OrderStatusText='已完成' where Id = @orderid
	end
	
	Select '{"retcode":0}' Result
End
Go