IF OBJECT_ID('P_OrderBuyNow') IS Not Null
	Drop Procedure P_OrderBuyNow
Go
Create procedure P_OrderBuyNow
(
	@token		nvarchar(50),
	@ucid	int,
	@gid		int,
	@number		int,
	@skuid		int
)
As
Begin
	Declare @ResultData nvarchar(max),@address nvarchar(max),@exist_address nvarchar(5),@goods_list nvarchar(max),
		@order_total_num int,@order_total_price numeric(18,2),@express_price numeric(18,2),@intra_region nvarchar(5),
		@order_pay_price numeric(18,2),@order_coupon_price numeric(18,2),@order_coupon nvarchar(max)
	Select @token = dbo.GetUserId(@token)
	Select @address = ''
	If Exists (Select 1 From [T_ShoppingAddress] where [cUserId] = @token And bDefault = 1)
	Begin
		Select @address = '{'+
							'"address_id":"'+CAST(Id as nvarchar)+'",'+
							'"name":"'+cPerson+'",'+
							'"phone":"'+cTelephone+'",'+
							'"province":"'+cProvince+'",'+
							'"city":"'+cCity+'",'+
							'"region":"'+cCounty+'",'+
							'"detail":"'+cAddress+'"'+
						'}'
		from [T_ShoppingAddress] where [cUserId] = @token And bDefault = 1
		
		Select @exist_address = 'true'
		Select @intra_region = 'true'
		
		Select @express_price = ISNULL(d.Price,d2.Price)
			from [T_ShoppingAddress] a
			Left Join T_DeliveryPrice d on a.cProvince = d.Province
			Left Join T_DeliveryPrice d2 on d2.Province='Ĭ��'
			where [cUserId] = @token And bDefault = 1
	End
	Else
	Begin
		Select @address = ''
		Select @exist_address = 'false'
		Select @intra_region = 'false'
		Select @express_price = 0
	End
	Select @goods_list=''
	Select @goods_list = '{'+
							'"goods_id":"'+CAST(d.GoodsId as Nvarchar)+'",'+
							'"imgurl":"'+d.ImgUrl+'",'+
							'"goods_name":"'+g.Title+'",'+
							'"goods_sku":"'+d.Name+'",'+
							'"goods_price":"'+CAST(d.Price as Nvarchar)+'",'+
							'"total_num":"'+CAST(@number as Nvarchar)+'"'+
						'}'
		from T_GoodsDetail d
		Inner Join T_Goods g on d.GoodsId = g.Id
		Where d.Id = @skuid
	
	Select @goods_list = '['+@goods_list+']'	
	
	Select @order_total_num = @number
	Select @order_total_price =ISNULL(SUM(Price * @number),0)
		from T_GoodsDetail 
		Where Id = @skuid
		
	
	select uc.Id,c.Title,c.Amount 
	into #temp
	from T_UserCoupon uc
	inner join T_Coupon c on c.Id = uc.CouponId
	inner join T_CouponLimit cl on uc.CouponId = cl.CouponId
	where uc.UserId = @token
		And (cl.GoodsId = 0 or cl.GoodsId = @gid)
		And (cl.SkuId = 0 or cl.SkuId = @skuid)
		And c.OverDate > GETDATE()
		And c.IsStopUsing = 0
		And c.LimitAmount <= @order_total_price
		And uc.IsUsed = 0
	Order By c.Amount desc
	
	select @order_coupon = ''
	select @order_coupon = @order_coupon + '{'+
		'"ucid":'+CAST(c.Id as nvarchar)+','+
		'"title":"'+c.Title+' ��'+ CAST(c.Amount as nvarchar) +'",'+
		'"amount":'+CAST(c.Amount as nvarchar)+''+
		'},'
		From #temp c
	select @order_coupon_price = Amount from (select top 1 Amount from #temp where (Id = @ucid or @ucid = 0)) t
	select @order_coupon_price = ISNULL(@order_coupon_price,0)
	Select @order_pay_price = @express_price + @order_total_price - @order_coupon_price
	
	if LEN(@order_coupon) > 0
		select @order_coupon = LEFT(@order_coupon,LEN(@order_coupon)-1)
	select @order_coupon = '['+@order_coupon+']'
		
	Select @ResultData = '{"address":'+@address+',"exist_address":'+@exist_address+',"goods_list":'+@goods_list+',
		"order_total_num":'+CAST(@order_total_num as nvarchar)+',"order_total_price":'+Cast(@order_total_price as nvarchar)+',
		"intra_region":'+@intra_region+',"express_price":'+Cast(@express_price as nvarchar)+',"order_pay_price":'+Cast(@order_pay_price as nvarchar)+',
		"order_coupon_price":'+CAST(@order_coupon_price as nvarchar)+',"order_coupon":'+@order_coupon+'}' 
	Select @ResultData Result
	drop table #temp
End
Go
