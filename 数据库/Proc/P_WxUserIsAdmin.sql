If OBJECT_ID('P_WxUserIsAdmin') Is Not Null
	Drop Procedure P_WxUserIsAdmin
Go

Create Procedure P_WxUserIsAdmin
(
	@token			nvarchar(200)
)
As
Begin
	select @token = dbo.GetUserId(@token)

	Declare @ResultData nvarchar(max)
	--Select @ResultData = '{"token":"token'+CAST([USER_ID] As Nvarchar)+'","user_id":"'+[USER_ID]+'"}' From T_WxUser Where openid = @openid
	Select @ResultData = '{"isadmin":'+CAST(IsAdmin As Nvarchar)+'}' From T_WxUser Where [USER_ID] = @token
	
	Select @ResultData Result
End
Go