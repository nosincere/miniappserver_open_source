IF OBJECT_ID('P_OrderDetailFahuo') IS Not Null
	Drop Procedure P_OrderDetailFahuo
Go
Create procedure P_OrderDetailFahuo
(
	@orderdetailids		nvarchar(400),
	@expressno			nvarchar(100),
	@expresscompany		nvarchar(100),
	@msg				nvarchar(100) output
)
As
Begin
	Select @msg=''
	If Exists(Select 1 From 
				T_OrderStatus os 
				Inner Join dbo.f_splitToTable(@orderdetailids,',') t on os.OrderDetailId = t.col
				Where ISNULL(os.ExpressNo,'')<>'')
	Begin
		Select @msg = '所选明细中存在已发货的明细'
	End
	Else If Exists(Select 1 From 
				T_OrderStatus os 
				Inner Join dbo.f_splitToTable(@orderdetailids,',') t on os.OrderDetailId = t.col
				Where os.DeliveryStatus <> 10)
	Begin
		Select @msg = '所选明细状态不为待发货'
	End				
	Else
	Begin
		--Select * From T_OrderStatus os
		--Inner Join dbo.f_splitToTable(@orderdetailids,',') t on os.OrderDetailId = t.col
	
		Declare @count int,@orderdetailcount int
		Update os Set os.ExpressNo=case when @expresscompany='Customer' then '' else @expressno end,ExpressCompany = @expresscompany,
			OrderStatus=30,OrderStatusText='待收货',DeliveryStatus=20,DeliveryStatusText='已发货',
			ReceiptStatus = 10,ReceiptStatusText='待收货'
		From T_OrderStatus os
		Inner Join dbo.f_splitToTable(@orderdetailids,',') t on os.OrderDetailId = t.col
		Select @count = @@ROWCOUNT
		
		Declare @orderid int
		Select @orderid = OrderId From T_OrderDetail od inner Join dbo.f_splitToTable(@orderdetailids,',') t on od.Id = t.col
		
		Select @count = COUNT(*) From T_OrderStatus where OrderId = @orderid
		Select @orderdetailcount = COUNT(*) From T_OrderStatus where OrderId = @orderid Group By OrderId,OrderStatus
		
		if @count = @orderdetailcount
			Update T_Order set OrderStatus=30,OrderStatusText='待收货' where Id = @orderid
		
		--
	End
	
End
Go
