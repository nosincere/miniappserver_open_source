If OBJECT_ID('P_AddCommentImg') Is Not Null
	Drop Procedure P_AddCommentImg
Go
Create Procedure P_AddCommentImg
(
	@orderdetailid		int,
	@imgurls			nvarchar(max)
)
As
Begin
	DECLARE @xmlHandle INT    
	EXEC sp_xml_preparedocument @xmlHandle OUTPUT, @imgurls   
	SELECT  ImgUrl,iOrder
	INTO    #TradePrice
	FROM    OPENXML(@xmlHandle,N'/Root/Record')   
	WITH( ImgUrl NVARCHAR(200), iOrder int)
	EXEC sp_xml_removedocument @xmlHandle
	
	Delete from T_CommentImage where OrderDetailId = @orderdetailid
	
	Insert Into T_CommentImage(OrderDetailId,ImgUrl,iOrder)
	select @orderdetailid,ImgUrl,iOrder
	From #TradePrice
End
Go