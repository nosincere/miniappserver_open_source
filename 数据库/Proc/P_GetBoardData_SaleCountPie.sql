If OBJECT_ID('P_GetBoardData_SaleCountPie') Is Not Null
	Drop Procedure P_GetBoardData_SaleCountPie
Go

Create Procedure P_GetBoardData_SaleCountPie
(
	@day	int
)
As
Begin
	declare @date date
	select @day = ISNULL(@day,30)
	Select @date = dateadd(day,-(@day-1),CONVERT(varchar(100), GETDATE(), 23)) 
	
	--legend
	Create Table #legend
	(
		GoodsId		int,
		GoodsIdStr	nvarchar(20),
		Name		Nvarchar(20),
		dAddDate	nvarchar(10),
		TotalName	nvarchar(200),
		SkuName		Nvarchar(20),
		TotalSkuName		Nvarchar(200),
		Total		numeric(10,2)
	)
	
	Insert Into #legend(GoodsId,GoodsIdStr,Name,TotalName,SkuName,TotalSkuName,Total,dAddDate)
	Select g.Id,'Goods' +CAST(g.Id as nvarchar), case when Len(Title) > 10 Then LEFT(Title,10) else Title end,Title,
		case when Len(gd.Name) > 10 Then LEFT(gd.Name,10) else gd.Name end,gd.Name,
		d.Number,d.dAddDate
	From T_Goods g
	Inner Join(
				Select GoodsId,GoodsSkuId, SUM(Number) AS Number,SUM(Price * Number) AS Total ,CONVERT(varchar(100),dAddTime, 23) as dAddDate
				from T_OrderDetail 
				where CONVERT(varchar(100),dAddTime, 23) >= @date
				Group By GoodsId,GoodsSkuId,CONVERT(varchar(100),dAddTime, 23)
		) d on g.Id = d.GoodsId
	Inner Join T_GoodsDetail gd on d.GoodsSkuId = gd.Id
	Order By d.Total desc

	select * from #legend 
	
	drop table #legend

End
Go

