IF OBJECT_ID('P_GetProductSkuList') IS Not Null
	Drop Procedure P_GetProductSkuList
Go
Create procedure P_GetProductSkuList
(
	@pageSize	int,
	@pageIndex	int,
	@sort		nvarchar(500),
	@order		nvarchar(500),
	@filter		nvarchar(500),
	@goodsid	int,
	@isdisplay	int,
	@total		int output
)
As
Begin
	Select g.Title GoodsTitle,g.Id as goodsid,d.Id,d.Name,d.costprice,d.Price,d.KuCun,
		d.ImgUrl,
		CONVERT(varchar(100), d.dAddTime, 23) +' ' + CONVERT(varchar(100), d.dAddTime, 108) dAddTime,
		case d.bDisplay when 1 then CONVERT(varchar(100), d.dDisplayTime, 23)+ ' ' + CONVERT(varchar(100), d.dDisplayTime, 108) else '' end dDisplayTime,
		d.iOrder,case d.bDisplay when 1 then '��' else '��' end as bDisplay,cast(d.bDisplay as int) bDisplayValue
	into #temp
	from T_GoodsDetail d 
	inner join T_Goods g on g.Id = d.GoodsId
	where (@goodsid = 0 or GoodsId = @goodsid )
		And (@isdisplay = 2 or (@isdisplay = 1 and d.bDisplay = 1) or (@isdisplay = 0 and d.bDisplay = 0))
	
	
		declare @records int
	select @sort = 'goodsid,'+@sort
	exec P_ToPage @TableName ='#temp',@searchField='GoodsTitle,ImgUrl,Name',@searchString=@filter,@searchOper='in',@PageSize=@pageSize,@PageIndex=@pageIndex,
		@PageTotal=@records output,@RecordTotal=@total output, @OrderByField=@sort,@OrderByType=@order
End
Go