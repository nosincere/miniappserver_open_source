If OBJECT_ID('P_GetBoardData_SaleAmountPie') Is Not Null
	Drop Procedure P_GetBoardData_SaleAmountPie
Go

Create Procedure P_GetBoardData_SaleAmountPie
(
	@day	int
)
As
Begin
	declare @date date
	select @day = ISNULL(@day,30)
	Select @date = dateadd(day,-(@day-1),CONVERT(varchar(100), GETDATE(), 23)) 
	
	--legend
	Create Table #legend
	(
		GoodsId		int,
		GoodsIdStr	nvarchar(20),
		Name		Nvarchar(20),
		dAddDate	nvarchar(10),
		TotalName	nvarchar(200),
		SkuName		Nvarchar(20),
		TotalSkuName		Nvarchar(200),
		Total		numeric(10,2)
	)
	
	Insert Into #legend(GoodsId,GoodsIdStr,Name,TotalName,SkuName,TotalSkuName,Total,dAddDate)
	Select g.Id,'Goods' +CAST(g.Id as nvarchar), case when Len(Title) > 10 Then LEFT(Title,10) else Title end,Title,
		case when Len(gd.Name) > 10 Then LEFT(gd.Name,10) else gd.Name end,gd.Name,
		d.Total,d.dAddDate
	From T_Goods g
	Inner Join(
				Select GoodsId,GoodsSkuId, SUM(Number) AS Number,SUM(Price * Number) AS Total ,CONVERT(varchar(100),dAddTime, 23) as dAddDate
				from T_OrderDetail 
				where CONVERT(varchar(100),dAddTime, 23) >= @date
				Group By GoodsId,GoodsSkuId,CONVERT(varchar(100),dAddTime, 23)
		) d on g.Id = d.GoodsId
	Inner Join T_GoodsDetail gd on d.GoodsSkuId = gd.Id
	Order By d.Total desc
	
	--Insert Into #legend(GoodsId,GoodsIdStr,Name,TotalName,Total,SkuName,TotalSkuName,dAddDate)
	--Select 0,'Goods0','其他','其他',ISNULL(SUM(Price * Number),0),'其他','其他',CONVERT(varchar(100),dAddTime, 23)
	--from T_OrderDetail 
	--where dAddTime >= @date
	--And GoodsId not in(select GoodsId from #legend)
	--Group By CONVERT(varchar(100),dAddTime, 23)
	--Order By SUM(Price * Number) desc
	
	--declare @othercount int
	--select @othercount = COUNT(*) From #legend where GoodsId=0
	--If @othercount < 7
	--Begin
	--	Insert Into #legend(GoodsId,GoodsIdStr,Name,TotalName,Total,SkuName,TotalSkuName,dAddDate)
	--	Select 0,'Goods0','其他','其他',0,'其他','其他',d.Name
	--	From #Date d
	--	Where d.Name not in(select dAddDate from #legend)
	--End

	select * from #legend 
	
	drop table #legend

End
Go

