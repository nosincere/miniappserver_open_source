If OBJECT_ID('P_UnifiedOrder') Is Not Null
	Drop Procedure P_UnifiedOrder
Go
Create Procedure P_UnifiedOrder
(
	@orderid int
)
As
Begin
	Declare @appid nvarchar(50),@mchid nvarchar(50),@body nvarchar(max),@openid nvarchar(50),@notifyurl nvarchar(100),@totalfee int,@ip nvarchar(100),@mchsecrect nvarchar(100),@out_trade_no nvarchar(50)
	Select @appid = AppId From T_MiniAppConfig where ConfigType='miniapp'
	Select @mchid = AppId,@mchsecrect = AppSecrect From T_MiniAppConfig where ConfigType='miniapppay'
	select @notifyurl = SubValue +'api/Process/Notify' from T_WxAppConfig where SubKey = 'HostName'
	select @ip = SubValue from T_WxAppConfig where SubKey = 'IP'
	
	select OrderId,Min(Id) as OrderDetailId Into #temporder From T_OrderDetail Group By OrderId
		
	select @body = '订单【'+ o.OrderNo +'】，'+ case when o.TotalNumber > 1 then g.Title + ' 等'+cast(o.TotalNumber AS nvarchar) + '商品' else g.Title end,@openid = u.openid,@totalfee = CAST(o.TotalAmount * 100 as int),
		@out_trade_no = o.OrderNo
	from T_Order o 
	inner join #temporder od on o.Id = od.OrderId
	Inner Join T_OrderDetail d on od.OrderDetailId = d.Id
	Inner Join T_Goods g on d.GoodsId = g.Id
	Inner Join T_WxUser u on o.UserId = u.[user_id]
	Where o.Id = @orderid
	
	Select @appid appid,@mchid mchid,@mchsecrect as mchsecrect,@body body,@openid openid,@notifyurl notifyurl,@totalfee totalfee,'小程序商城下单' as [attach],@ip as ip,@out_trade_no as out_trade_no
End
Go