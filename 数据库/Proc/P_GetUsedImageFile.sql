--获取在系统中有使用到的图片
IF OBJECT_ID('P_GetUsedImageFile') IS Not Null
	Drop Procedure P_GetUsedImageFile
Go
Create procedure P_GetUsedImageFile

As
Begin
	Create Table #Img(Name nvarchar(200))
	
	declare @sql nvarchar(max)
	
	select @sql=''
	select @sql=@sql +
		'select dbo.f_GetStrArrayStrOfIndex ('+ColumnName+', ''/'',-1) from ' + TableName +' union all '
		From T_UsedFileConfig
		
	if LEN(@sql)>0
	begin
		Select @sql = LEFT(@sql,LEN(@sql) - 10)
		select @sql='insert into #Img(Name) ' + @sql +' delete from #Img where ISNULL(Name,'''')='''''
	end
	print @sql
	--exec @sql
	EXEC SP_EXECUTESQL @sql
	
	Select Name From #Img
	Drop Table #Img
End
Go

