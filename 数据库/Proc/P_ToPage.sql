IF OBJECT_ID('P_ToPage') IS Not Null
	Drop Procedure P_ToPage
Go
CREATE PROCEDURE P_ToPage
(
	@TableName NVARCHAR(4000),--临时表名字
	@searchField	nvarchar(500),
	@searchString	nvarchar(500),
	@searchOper		nvarchar(500),--eq等于、ne不等于、lt小于、le小于等于、gt大于、ge大于等于、nu不存在 searchString为空、nn存在	searchString为空、in属于、ni不属于
	@PageSize INT,--每页显示数量
	@PageIndex INT,--从第几条数据开始 从0开始
	@PageTotal INT OUTPUT,--共有页数据
	@RecordTotal INT OUTPUT,--共有多少条数据
	@OrderByField NVARCHAR(Max),--根据什么排序进行分页,field1,field2,如果传入''，则传入RowNum列identidy(int,1,1)
	@OrderByType	nvarchar(max),
	@sum_fields NVARCHAR(Max) = '', --需要统计合计数据的字段名，格式为'field1;field2;field3'
	@rate_field NVARCHAR(Max) = '', --扩展合计公式,这边直接使用,也可用作毛利合计公式
	@all_field NVARCHAR(100) = '' --返回此列所有的数据，返回的数据逗号分隔   
)
AS 
SET NOCOUNT ON
DECLARE @SQL NVARCHAR(MAX)
if ISNULL(@searchString,'')=''
	select @searchField=''
If ISNULL(@searchField,'')<>''
begin
	declare @sqlmax nvarchar(max)
	if @searchOper='eq'
		select @sqlmax = 'select * from ' +@TableName+' where ' + @searchField +'='''+@searchString+''''
	else if @searchOper='ne'
		select @sqlmax = 'select * from ' +@TableName+' where ' + @searchField +'<>'''+@searchString+''''
	else if @searchOper='lt'
		select @sqlmax = 'select * from ' +@TableName+' where ' + @searchField +'<'''+@searchString+''''
	else if @searchOper='le'
		select @sqlmax = 'select * from ' +@TableName+' where ' + @searchField +'<='''+@searchString+''''
	else if @searchOper='gt'
		select @sqlmax = 'select * from ' +@TableName+' where ' + @searchField +'>'''+@searchString+''''
	else if @searchOper='ge'
		select @sqlmax = 'select * from ' +@TableName+' where ' + @searchField +'>='''+@searchString+''''
	else if @searchOper='nu'
		select @sqlmax = 'select * from ' +@TableName+' where ' + @searchField +' is null'
	else if @searchOper='nn'
		select @sqlmax = 'select * from ' +@TableName+' where ' + @searchField +' is not null'
	else if @searchOper='in'
	Begin
		--特殊处理BootstrapTable分页
		if CHARINDEX(',',@searchField) > 0
		Begin
			Select col 
			Into #fields
			From dbo.f_splitToTable(@searchField,',') 
			
			Declare @f nvarchar(max)
			select @f=''
			Select @f =' ' + @f + ' '+
				col + ' ' +
				'in('''+@searchString+''') '+
				'or ' +col + ' like ' +
				'''%'+@searchString+'%'' ' +
				'or'
			From #fields
			if LEN(@f) > 0
				Select @f = LEFT(@f,LEN(@f) - 2)
			
			select @sqlmax = 'select * from ' +@TableName+' where ' + @f
		End
		Else
			select @sqlmax = 'select * from ' +@TableName+' where ' + @searchField +' in('''+@searchString+''') or '+@searchField+' like ''%'+@searchString+'%'''
	End
	else if @searchOper='ni'
		select @sqlmax = 'select * from ' +@TableName+' where ' + @searchField +' not in('''+@searchString+''')'
	print  @sqlmax
	select @TableName='(' + @sqlmax +') t'

end
  
DECLARE @EndIndex INT,@BeginIndex int
SELECT  @EndIndex = @PageIndex * @PageSize 
IF @PageIndex IS NULL OR @PageIndex < 0 
  SET @PageIndex = 0 --默认第一条数据
select @BeginIndex = (@PageIndex -1) * @PageSize
IF @PageSize IS NULL
  OR @PageSize <= 0 
  SET @PageSize = 20 --默认每页50条
     
    IF @sum_fields IS NULL 
        SET @sum_fields = ''
    IF @OrderByField <> '' 
        BEGIN
	--创建新临时表#TempDataContainerToPage并增加自增列字段RowNum
--            SET @SQL = '  SELECT *,ROW_NUMBER() OVER (ORDER BY '
--                + @OrderByField
--                + ') as  RowNum into #TempDataContainerToPage from  '
--                + @TableName 
    --返回所有数据行数
            SET @SQL = '  Select @RowCounts = Count(1) From 	'
                + @TableName
                --select @TableName
                --return
    --返回指定页的数据
            SET @SQL = @SQL + '  Select  *  from  (select *,ROW_NUMBER() OVER (ORDER BY '+@OrderByField + ' ' + @OrderByType + ' ) as  RowNum  from  '
                + @TableName 
						+')Con where Con.RowNum >@BeginIndex And RowNum <=@EndIndex'
	--删除临时表
            --SET @SQL = @SQL + '  Drop Table #TempDataContainerToPage'				
			SET @SQL = @SQL + '  Select @PageTotal =Case When (@RowCounts % @PageSize) = 0 Then (@RowCounts / @PageSize) Else (@RowCounts / @PageSize) + 1 End 	'
            EXEC sp_executesql @SQL, N'@RowCounts INT OUTPUT,@PageTotal int output,@BeginIndex Int,@EndIndex Int,@PageSize int',
                @RecordTotal OUTPUT,@PageTotal output,@BeginIndex,@EndIndex,@PageSize
        END
    
    ELSE 
        BEGIN
			
            SET @SQL = '  Select * from ' + @TableName + '  where RowNum >@BeginIndex And RowNum <= @EndIndex'
            SET @SQL = @SQL + '  Select @RowCounts = Count(1) From 	'
                + @TableName
             
            SET @SQL = @SQL + '  Select @PageTotal = Case When (@RowCounts % @PageSize) = 0 Then (@RowCounts / @PageSize) Else (@RowCounts / @PageSize) + 1 End 	'
            EXEC sp_executesql @SQL, N'@RowCounts INT OUTPUT,@PageTotal int output,@BeginIndex Int,@EndIndex Int,@PageSize int',
                @RecordTotal OUTPUT,@PageTotal output,@BeginIndex,@EndIndex,@PageSize
        END

IF @sum_fields <> ''
  AND @PageIndex = 0 
  BEGIN
    DECLARE @i INT
    DECLARE @field NVARCHAR(40) 
    SELECT  @SQL = ''
    WHILE LEN(@sum_fields) > 0 
      BEGIN
		
        SELECT  @i = CHARINDEX(';', @sum_fields) 
        IF @i = 0 
          SET @field = @sum_fields
        ELSE 
          SET @field = SUBSTRING(@sum_fields, 1, @i - 1)
			
        SET @SQL = @SQL + ',sum(isnull(' + @field + ',0)) as ' + @field
									            
        IF @i = 0 
          BREAK
        ELSE 
          SET @sum_fields = SUBSTRING(@sum_fields, @i + 1, LEN(@sum_fields) - @i) 		               	
      END
    IF ISNULL(@rate_field, '') <> '' 
      SET @SQL = @SQL + ',' + @rate_field
    IF ISNULL(@all_field, '') <> '' 
      SET @SQL = @SQL + ',STUFF((SELECT '','' + cast(' + @all_field + ' as nvarchar) FROM ' + @TableName
              + ' FOR XML PATH('''')), 1, 1, '''') AS ' + @all_field
	
    SELECT  @SQL = 'select ''合计行'' as Row ' + @SQL + ' from ' + @TableName
    EXEC sp_executesql @SQL
  END
ELSE 
  IF @sum_fields = ''
    AND @PageIndex = 0
    AND ISNULL(@all_field, '') <> '' 
    BEGIN
      SELECT  @SQL = 'select ''合计行'' as Row , STUFF((SELECT '','' + cast(' + @all_field + ' as nvarchar) FROM ' + @TableName
              + ' FOR XML PATH('''')), 1, 1, '''') AS ' + @all_field
      EXEC sp_executesql @SQL
    END 

GO