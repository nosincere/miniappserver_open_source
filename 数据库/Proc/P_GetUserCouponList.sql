If OBJECT_ID('P_GetUserCouponList') Is Not Null
	Drop Procedure P_GetUserCouponList
Go

Create Procedure P_GetUserCouponList
(
	@type			nvarchar(200),
	@token			nvarchar(200)
)
As
Begin
		
	Declare @ResultData nvarchar(max),@couponList nvarchar(max)
	Select @token = dbo.GetUserId(@token)
	select @couponList = ''
	if @type = 'me'
	begin
		select uc.Id as ucid,uc.IsUsed,c.Id as cid,c.Title,c.Amount,CreateDate,OverDate,LimitAmount 
		into #t
		from T_UserCoupon uc
		inner join T_Coupon c on uc.CouponId = c.Id
		where uc.UserId = @token
		order by case when c.OverDate>GETDATE() then uc.IsUsed else uc.UserId end asc,c.OverDate desc
		select @couponList = @couponList + '{' + 
			'"ucid":' + CAST(ucId as nvarchar) + ','+
			'"isused":' + CAST(IsUsed as nvarchar) + ','+
			'"usedclass":"' + case when IsUsed = 1 or c.OverDate<GETDATE() then 'used' else '' end + '",'+
			'"cid":' + CAST(cId as nvarchar) + ','+
			'"title":"'+ c.Title+'",'+
			'"name":"'+ c.Title+'",'+
			'"amount":'+CAST(c.Amount as nvarchar) + ','+
			'"createdate":"'+CONVERT(varchar(100), CreateDate, 23)+ ' ' + CONVERT(varchar(100), CreateDate, 108) +'",'+
			'"overdate":"'+CONVERT(varchar(100), OverDate, 23)+ ' ' + CONVERT(varchar(100), OverDate, 108) +'",'+
			'"limitamount":'+cast(LimitAmount as nvarchar) + '},'
		from #t c
		drop table #t
		
	end
	else if @type='share'
	begin
		select @couponList = @couponList + '{' + 
			'"ucid":0,'+
			'"isused":0,'+
			'"usedclass":"",'+
			'"cid":' + CAST(c.Id as nvarchar) + ','+
			'"title":"'+ c.Title+'",'+
			'"name":"'+ c.Title+'",'+
			'"amount":'+CAST(c.Amount as nvarchar) + ','+
			'"createdate":"'+CONVERT(varchar(100), CreateDate, 23)+ ' ' + CONVERT(varchar(100), CreateDate, 108) +'",'+
			'"overdate":"'+CONVERT(varchar(100), OverDate, 23)+ ' ' + CONVERT(varchar(100), OverDate, 108) +'",'+
			'"limitamount":'+cast(LimitAmount as nvarchar) + '},'
		from T_Coupon c
		order by c.OverDate desc
	end
	if LEN(@couponList)>0
		select @couponList = LEFT(@couponList,LEN(@couponList)-1)
	Select @couponList = '['+@couponList+']'
	Select @ResultData = '{"couponList":' + @couponList + '}'
	Select @ResultData Result
End
Go
