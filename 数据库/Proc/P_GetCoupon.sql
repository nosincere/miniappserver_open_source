
--领取优惠券
If OBJECT_ID('P_GetCoupon') Is Not Null
	Drop Procedure P_GetCoupon
Go
Create Procedure P_GetCoupon
(
	@randomid	nvarchar(100),
	@token nvarchar(100),
	@couponid	int,
	@msg nvarchar(100) output
)
AS
Begin
	Select @token = dbo.GetUserId(@token)
	If Exists(Select 1 From T_CouponShare where RandomId = @randomid)
	Begin
		If Exists(Select 1 From T_CouponShare where RandomId = @randomid And ShareType = 1)
		Begin
			If Exists(Select 1 From T_CouponShare where RandomId = @randomid And ShareType = 1 And IsGet = 1)
				Goto CantGet
		End
	End
	Else
		Goto NotExists
	If Exists(Select 1 from T_Coupon where Id = @couponid And OverDate<GETDATE())
		goto OverDate
	If Exists(Select 1 From T_UserCoupon where UserId = @token and CouponId = @couponid)
		Goto Getted
	If Not Exists(Select 1 from T_Coupon where Id = @couponid)
		goto NotExists
	If Not Exists(Select 1 from T_Coupon where Id = @couponid And RemainCount > 0)
		Goto NoRemainCout
	
	Declare @row int
	
	Update T_Coupon Set RemainCount = RemainCount - 1 where Id = @couponid And RemainCount > 0
	Select @row = @@ROWCOUNT
	if @row = 1
	Begin
		Insert Into T_UserCoupon(UserId,CouponId)
		Select @token,@couponid
		
		Update T_CouponShare Set IsGet = 1 Where RandomId = @randomid
	End
	Else
		Goto NoRemainCout
	Select @msg='领取成功'	
	return
	
	OverDate:
		Select @msg = '对不起，优惠券已失效'
		return
	NotExists:
		Select @msg = '对不起，优惠券不存在' 
		return
	NoRemainCout:
		Select @msg = '对不起，优惠券已抢光' 
		return
	Getted:
		Select @msg = '对不起，无法重复领取'
		return
	CantGet:
		Select @msg = '对不起，优惠券转发无效'
		return
End
Go