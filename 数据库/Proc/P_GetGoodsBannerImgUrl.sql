If OBJECT_ID('P_GetGoodsBannerImgUrl') is not null
	Drop procedure P_GetGoodsBannerImgUrl
Go
Create Procedure P_GetGoodsBannerImgUrl
(
	@goodsid int
)
AS
Begin
	Declare @content nvarchar(max)
	select @content = ''
	Select @content = @content + '<p><img src="' + ImgUrl + '" _src="'+ImgUrl+'"/></p>' from T_GoodsBannerImage where GoodsId = @goodsid
	select @content as content
End
Go
