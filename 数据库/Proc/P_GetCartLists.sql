If OBJECT_ID('P_GetCartLists') Is Not Null
	Drop Procedure P_GetCartLists
Go

Create Procedure P_GetCartLists
(
	@token			nvarchar(200)
)
As
Begin
		
	Declare @ResultData nvarchar(max),@goods_list nvarchar(max),@order_total_price	numeric(10,2),@userid nvarchar(20),@selectall int

	Select @userid = dbo.GetUserId(@token)
	
	Select @goods_list=''
	Select @goods_list = @goods_list +
		'{'+
			'"id":'+CAST(c.id as nvarchar)+','+
			'"checked":'+CAST(c.checked as nvarchar)+','+
			'"goods_id":'+CAST(g.Id as nvarchar)+','+
			'"txtStyle":"txtStyle",'+
			'"goods_name":"'+g.Title+'",'+
			'"goods_sku":{"goods_attr":"'+d.Name+'"},'+
			'"goods_price":'+CAST(d.Price as nvarchar)+','+
			'"total_num":'+CAST(c.Number as nvarchar)+','+
			'"goods_sku_id":'+CAST(d.Id as nvarchar)+','+
			'"image":[{"file_path":"'+d.ImgUrl+'"}]'+
		'},'
		From T_CartList c 
		Inner Join T_GoodsDetail d on d.Id = c.GoodsSkuId
		Inner Join T_Goods g on c.GoodsId = g.Id
		Where c.UserId = @userid
	IF LEN(@goods_list) > 0
		Select @goods_list = LEFT(@goods_list,LEN(@goods_list)-1)
	Select @goods_list = '['+ISNULL(@goods_list,'')+']'
	Select @order_total_price =	ISNULL(SUM(Total),0) from(
									Select c.Number * d.Price as Total 
									from T_CartList c 
									Inner Join T_GoodsDetail d on d.Id = c.GoodsSkuId	
									Where c.UserId = @userid and c.checked=1
								) t	

	if exists(select 1 from T_CartList where UserId = @userid and checked=0)	
		Select @selectall = 0
	else
		select @selectall = 1
			
	Select @ResultData = '{"goods_list":' + @goods_list + ',"order_total_price":'+Cast(@order_total_price as nvarchar)+',"selectall":'+CAST(@selectall as nvarchar)+'}'
	Select @ResultData Result
End
Go

