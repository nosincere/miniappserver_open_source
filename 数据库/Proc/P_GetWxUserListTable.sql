If OBJECT_ID('P_GetWxUserListTable') Is Not Null
	Drop Procedure P_GetWxUserListTable
Go
Create Procedure P_GetWxUserListTable
(
	@pageSize	int,
	@pageIndex	int,
	@sort		nvarchar(500),
	@order		nvarchar(500),
	@filter		nvarchar(500),
	@total		int output
)
As
Begin
	Select c.user_id, c.nickName,country +' ' + c.province+ ' '+c.city as Area,c.avatarUrl,case c.IsAdmin when 1 then '��' else '��' end as IsAdmin,
		a.cPerson,a.cTelephone,a.cProvince + a.cCity + a.cCounty + a.cAddress as cAddress,case a.bDefault when 1 then '��' else '��' end as cDefaultAddress
	Into #temp
	From T_WxUser c
	Left Join T_ShoppingAddress a on c.user_id = a.cUserId
	declare @records int
	
	exec P_ToPage @TableName ='#temp',@searchField='nickName,Area',@searchString=@filter,@searchOper='in',@PageSize=@pageSize,@PageIndex=@pageIndex,
		@PageTotal=@records output,@RecordTotal=@total output, @OrderByField=@sort,@OrderByType=@order
End
Go