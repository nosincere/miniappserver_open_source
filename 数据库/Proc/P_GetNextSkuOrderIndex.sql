IF OBJECT_ID('P_GetNextSkuOrderIndex') IS Not Null
	Drop Procedure P_GetNextSkuOrderIndex
Go
Create procedure P_GetNextSkuOrderIndex
(
	@goodsid	int
)
As
Begin
	declare @maxindex int,@count int
	If exists(select 1 from T_GoodsDetail where GoodsId = @goodsid)
	Begin
		select @maxindex = ISNULL(MAX(iOrder),0) + 1 From T_GoodsDetail where GoodsId = @goodsid
		select @count = ISNULL(count(iOrder),0) + 1 From T_GoodsDetail where GoodsId = @goodsid
		if(@maxindex > @count)
			return @maxindex
		else
			return @count
	End
	Else
	begin
		return 1
	end
End
Go