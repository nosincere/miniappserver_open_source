If OBJECT_ID('P_GetGoodsList') Is Not Null
	Drop Procedure P_GetGoodsList
Go

Create Procedure P_GetGoodsList
(
	@page			int,--��1,2,3ҳ
	@sortType		nvarchar(20),--all,sales,price
	@sortPrice		int,
	@categoryid		int,
	@search			nvarchar(200)
)
As
Begin
		
	Declare @ResultData nvarchar(max),@sql nvarchar(max),@list nvarchar(max),@goodsids nvarchar(500),@pageSize int
	
	select @pageSize = 10
	
	select @goodsids = GoodsIds From T_Category where Id=@categoryid
	select col
	into #g
	from dbo.f_splitToTable(@goodsids,',')
	
	Select * 
	Into #Goods
	From T_Goods g
	inner join #g g2 on g.Id = case when @categoryid=0 then g.Id else g2.col end
	
	Create Table #temp
	(
		IdIndex		int identity(1,1),
		GoodsId		int,
		SkuId		int,
		SkuName		nvarchar(200),
		GoodsName	nvarchar(200),
		Price		Numeric(18,2),
		LinePrice	numeric(18,2),
		ImgUrl		nvarchar(500)
	)
	
	Select @sql='Insert Into #temp(SkuName,GoodsName,Price,LinePrice,ImgUrl,GoodsId,SkuId) '+
	'Select d.Name,g.Title,d.Price,d.costprice,d.imgurl,g.Id,d.Id '+
	'From T_GoodsDetail d '+
	'Inner Join #Goods g on d.GoodsId = g.Id '+
	'Left Join ( '+
	'	Select SUM(Number) as salenumber,GoodsSkuId From T_OrderDetail Group BY GoodsSkuId '+
	'	) sale on d.Id = sale.GoodsSkuId '+
	'Where g.Title like ''%'+@search+'%'' '+
	'Order By Case @sortType When ''all'' then d.Id when ''sales'' then ISNULL(sale.salenumber,0) when ''price'' then d.Price else d.Id end '+
	Case When @sortPrice = 1 then 'desc' else 'asc' end
	print @SQL
	EXEC sp_executesql @SQL, N'@sortType nvarchar(20)',@sortPrice
                
	Select @list = ''
	
	Select * Into #ResultPager From(select *,ROW_NUMBER() OVER(order by IdIndex) RowNum from #temp) t Where t.RowNum between (@page - 1)*@pageSize + 1 and @page * @pageSize
	
	select @list = @list + '{'+
		'"imgurl":"'+ImgUrl+'",'+
		'"goods_name":"'+GoodsName+'",'+
		'"price":'+Cast(Price as nvarchar)+','+
		'"goods_id":'+Cast(GoodsId as nvarchar)+','+
		'"sku_id":'+Cast(SkuId as nvarchar)+','+
		'"line_price":'+CAST(LinePrice as nvarchar)+','+
		'"skuname":"'+SkuName+'"'+
	'},'
	From #ResultPager
	If LEN(@list) >0
		Select @list = LEFT(@list,LEN(@list) - 1)
	Select @list = '['+@list+']'
	
	Select @ResultData = '{"list":{"data":'+@list+'}}'
	Select @ResultData Result
	
	drop table #Goods
	drop table #temp
End
Go
