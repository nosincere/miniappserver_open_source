If OBJECT_ID('P_DeleteGoodsDetail') Is Not Null
	Drop Procedure P_DeleteGoodsDetail
Go
Create Procedure P_DeleteGoodsDetail
(
	@id		int,
	@msg	nvarchar(50) output
)
As
Begin
	Select @msg = ''
	If exists(select 1 from T_OrderDetail where GoodsSkuId = @id)
	Begin
		Select @msg = '当前商品价格已有订单，不能删除，只能下架。'
	End
	Else
	Begin
		Delete From T_GoodsDetail where Id = @id
	End
End
Go