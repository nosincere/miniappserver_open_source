If OBJECT_ID('P_UserLogin') Is Not Null
	Drop Procedure P_UserLogin
Go

Create Procedure P_UserLogin
(
	@openid			nvarchar(200),
	@nickName		nvarchar(200),
	@gender			int,
	@language		nvarchar(200),
	@city			nvarchar(200),
	@province		nvarchar(200),
	@country		nvarchar(200),
	@avatarUrl		nvarchar(400)
)
As
Begin
	If Exists(Select 1 From T_WxUser where openid = @openid)
		Update T_WxUser 
		Set nickName=@nickName,gender=@gender,[language]=@language,city=@city,province=@province,country=@country,avatarUrl=@avatarUrl
		Where openid = @openid
	Else
		Insert Into T_WxUser(openid,nickName,gender,[language],city,province,country,avatarUrl)
		Select @openid,@nickName,@gender,@language,@city,@province,@country,@avatarUrl
		
	Declare @ResultData nvarchar(max)
	--Select @ResultData = '{"token":"token'+CAST([USER_ID] As Nvarchar)+'","user_id":"'+[USER_ID]+'"}' From T_WxUser Where openid = @openid
	Select @ResultData = '{"token":"'+RIGHT(sys.fn_varbintohexstr(HASHBYTES('MD5','!((#token' + CAST([user_id] as nvarchar) )),32)+'","user_id":"'+CAST([USER_ID] As Nvarchar)+'"}' From T_WxUser Where openid = @openid
	
	Select @ResultData Result
End
Go