IF OBJECT_ID('P_SaveWxHelpDoc') IS Not Null
	Drop Procedure P_SaveWxHelpDoc
Go
Create procedure P_SaveWxHelpDoc
(
	@docXml	nvarchar(max),
	@msg	nvarchar(100) output
)
As
Begin
	DECLARE @xmlHandle INT    
	EXEC sp_xml_preparedocument @xmlHandle OUTPUT, @docXml   
	SELECT  Title,Content
	INTO    #temp
	FROM    OPENXML(@xmlHandle,N'/Root/Record')   
	WITH( Title NVARCHAR(100), Content NVARCHAR(4000))
	EXEC sp_xml_removedocument @xmlHandle
	
	Delete From T_WxHelp
	Delete From #temp where ISNULL(Title,'')='' And ISNULL(Content,'')=''
	
	Insert Into T_WxHelp(Title,Content)
	Select Title,Content From #temp
	
	Select @msg = ''
	
	Drop table #temp
End
Go