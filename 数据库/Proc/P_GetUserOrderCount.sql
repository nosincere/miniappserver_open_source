If OBJECT_ID('P_GetUserOrderCount') Is Not Null
	Drop Procedure P_GetUserOrderCount
Go

Create Procedure P_GetUserOrderCount
(
	@token			nvarchar(200)
)
As
Begin
		
	Declare @ResultData nvarchar(max),@orderCount nvarchar(max),@paymentcount	int,@deliverycount int,@receivedcount int,@all int
	Select @token = dbo.GetUserId(@token)
	Select @all=COUNT(*) From T_Order Where UserId = @token
	Select @paymentcount=COUNT(*) From T_Order Where UserId = @token And OrderStatus=10
	Select @deliverycount=COUNT(*) From T_Order Where UserId = @token And OrderStatus = 20
	Select @receivedcount=COUNT(*) From T_Order Where UserId = @token And OrderStatus = 30
	Select @orderCount = '{"all":'+CAST(@all as nvarchar)+',"payment":'+CAST(@paymentcount As Nvarchar)+',"delivery":'+CAST(@deliverycount As Nvarchar)+',"received":'+CAST(@receivedcount as nvarchar)+'}' 
	Select @ResultData = '{"orderCount":' + @orderCount + '}'
	Select @ResultData Result
End
Go