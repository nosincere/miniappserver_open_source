If OBJECT_ID('P_AddLog') Is Not Null
	Drop Procedure P_AddLog
Go
Create Procedure P_AddLog
(
	@logtype	nvarchar(20),
	@function	nvarchar(50),
	@requestpara	nvarchar(max),
	@responsedata	nvarchar(max),
	@errmsg			nvarchar(max),
	@stacktrace		nvarchar(max)
)
As
Begin
	Insert Into T_Log(LogType,[Function],RequestPara,ResponseData,ErrMsg,StackTrace)
	select @logtype,@function,@requestpara,@responsedata,@errmsg,@stacktrace
End
Go