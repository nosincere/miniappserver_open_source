If OBJECT_ID('P_WxMiniAppUserSignIn') Is Not Null
	Drop Procedure P_WxMiniAppUserSignIn
Go
--用户签到，签到时间以服务器时间为准
Create Procedure P_WxMiniAppUserSignIn
(
	@token	nvarchar(50)
)
As
Begin
	Select @token = dbo.GetUserId(@token)
	Declare @continueday int,@point int
	print @token
	Select @continueday  = dbo.GetUserContinueSignDay(@token)
	If @continueday = 0
		Select @point = 1
	Else If @continueday < 7 
		Select @point = @continueday + 1
	Else
		Select @point = 7
	If Not Exists(Select 1 From T_WxMiniAppUserPoiontsLog WHere UserId =@token And SignDate = CONVERT(varchar(100), GETDATE(), 23) And [Type] = 'sign')
	Begin
		Insert Into T_WxMiniAppUserPoiontsLog(UserId,[Type],Points,SignDate,RelationId,Comment)
		Select @token,'sign',@point,CONVERT(varchar(100), GETDATE(), 23),0,'签到'
		
		If Not Exists (Select 1 From T_WxMiniAppUserPoints Where UserId = @token) 
		Begin
			Insert Into T_WxMiniAppUserPoints(UserId,Points)
			Select @token,@point
		End
		Else 
			Update T_WxMiniAppUserPoints Set Points = Points + @point Where UserId = @token
		
		Select '{"code":0,"msg":"已签到,积分+'+CAST(@point as nvarchar)+'","points":'+CAST(@point as nvarchar)+'}' Result
	End
	ELse
		Select '{"code":-1,"msg":"已签到"}' Result
End
Go
