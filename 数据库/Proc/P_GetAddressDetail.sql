IF OBJECT_ID('P_GetAddressDetail') IS Not Null
	Drop Procedure P_GetAddressDetail
Go
Create procedure P_GetAddressDetail
(
	@id		nvarchar(20)
)
As
Begin
	Declare @ResultData nvarchar(max),@list nvarchar(max)

	Select @list = ''
	Select @list = '{'+
						'"name":"'+cPerson+'",'+
						'"phone":"'+cTelephone+'",'+
						'"province":"'+cProvince+'",'+
						'"city":"'+cCity+'",'+
						'"region":"'+cCounty+'",'+
						'"detail":"'+cAddress+'",'+
						'"address_id":"'+CAST(Id as nvarchar)+'",'+
						'"region":"'+cProvince+','+cCity+','+cCounty+'"'+
					'}'
	from [T_ShoppingAddress] where Id = @id


	
	Select @ResultData = '{"detail":'+@list+'}' 
	Select @ResultData Result
End
Go
