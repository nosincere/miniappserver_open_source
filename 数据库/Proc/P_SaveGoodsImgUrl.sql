IF OBJECT_ID(N'P_SaveGoodsImgUrl') IS NOT NULL 
	DROP PROCEDURE P_SaveGoodsImgUrl
GO

CREATE PROCEDURE P_SaveGoodsImgUrl  
(
	@goodsid	int,
	@filexml	nvarchar(max)
)
As
Begin
	DECLARE @xmlHandle INT    
	EXEC sp_xml_preparedocument @xmlHandle OUTPUT, @filexml   
	SELECT  FileUrl,iOrder
	INTO    #File
	FROM    OPENXML(@xmlHandle,N'/Root/Record')   
	WITH(FileUrl nvarchar(200),iOrder int)
	EXEC sp_xml_removedocument @xmlHandle

	If Exists(select 1 From T_GoodsBannerImage where GoodsId=@goodsid)
		Delete From T_GoodsBannerImage where GoodsId=@goodsid
	
	Insert Into T_GoodsBannerImage(GoodsId,ImgUrl,iOrder)
	Select @goodsid,FileUrl,iOrder
	From #File
	where ISNULL(FileUrl,'')<>''
End
Go