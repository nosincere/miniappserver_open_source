If OBJECT_ID('P_ShareCoupon') Is Not Null
	Drop Procedure P_ShareCoupon
Go
Create Procedure P_ShareCoupon
(
	@token		nvarchar(50),
	@random		nvarchar(20),
	@couponid	int
)
AS
Begin
	Declare @ShareType int
	select @ShareType = SubValue from T_WxAppConfig where SubKey='AllowMultiShareCoupon'
	select @token = dbo.GetUserId(@token)
	insert into T_CouponShare(RandomId,ShareType,UserId,IsGet,CouponId)
	select @random,@ShareType,@token,0,@couponid
	
	select '{"retcode":0,"msg":"success"}' Result
End
Go