If OBJECT_ID('P_WxMiniAppUserPointsLog') Is Not Null
	Drop Procedure P_WxMiniAppUserPointsLog
Go
--���ּ�¼
Create Procedure P_WxMiniAppUserPointsLog
(
	@token	nvarchar(50)
)
As
Begin
	Select @token = dbo.GetUserId(@token)
	Declare @result nvarchar(max),@totalpoinots int
	
	Select @result = ''
	
	Select @result = @result + '{' +
			'"date":"'+CreateDate+'",'+
			'"points":'+CAST(Points as nvarchar)+','+
			'"comment":"'+Comment+'",'+
			'"type":"'+[Type]+'",'+
			'"relationid":'+CAST(RelationId as nvarchar)+''+
		'},'
	From T_WxMiniAppUserPoiontsLog
	Where UserId = @token
		Order By CreateDate Desc
		
	If LEN(@result)	> 0
		Select @result = LEFT(@result,LEN(@result) - 1)
	
	Select @totalpoinots = Points From T_WxMiniAppUserPoints Where UserId = @token
	Select @totalpoinots = ISNULL(@totalpoinots,0)
	
	Select '{"totalpoints":'+CAST(@totalpoinots as nvarchar)+',"record":['+@result+']}' Result
End
Go
