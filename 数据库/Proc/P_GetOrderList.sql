IF OBJECT_ID('P_GetOrderList') IS Not Null
	Drop Procedure P_GetOrderList
Go
Create procedure P_GetOrderList
(
	@pageSize	int,
	@pageIndex	int,
	@sort		nvarchar(500),
	@order		nvarchar(500),
	@filter		nvarchar(500),
	@orderstatus	int,
	@total		int output
)
As
Begin
	select OrderId,Min(Id) as OrderDetailId Into #temporder From T_OrderDetail Group By OrderId

	select o.Id, case when o.TotalNumber > 1 then g.Title + ' ��'+cast(o.TotalNumber AS nvarchar) + '��Ʒ' else g.Title end as Title,
		o.OrderNo,o.OrderStatusText,o.TotalAmount,o.TotalNumber,o.OrderStatus,
		CONVERT(varchar(100), o.dAddTime, 23)+ ' ' + CONVERT(varchar(100), o.dAddTime, 108) as dAddTime,
		del.cPerson,del.cTelephone as cTel,del.cProvince + del.cCity + del.cCounty + del.cAddress as cAddress,Comment
	Into #temp
	from T_Order o 
	inner join #temporder od on o.Id = od.OrderId
	Inner Join T_OrderDetail d on od.OrderDetailId = d.Id
	Inner Join T_Goods g on d.GoodsId = g.Id
	Inner Join T_GoodsDetail gd on d.GoodsSkuId = gd.Id
	inner Join T_Delivery del on o.Id = del.OrderId
	where @orderstatus=0 or o.OrderStatus = @orderstatus
	order by o.dAddTime desc
	
	declare @records int
	
	exec P_ToPage @TableName ='#temp',@searchField='Title,OrderNo,OrderStatusText,cPerson,cTel,cAddress,Comment,dAddTime',@searchString=@filter,@searchOper='in',@PageSize=@pageSize,@PageIndex=@pageIndex,
		@PageTotal=@records output,@RecordTotal=@total output, @OrderByField=@sort,@OrderByType=@order
End
Go
