If OBJECT_ID('P_CartDecAndAdd') Is Not Null
	Drop Procedure P_CartDecAndAdd
Go

Create Procedure P_CartDecAndAdd
(
	@token			nvarchar(200),
	@type			nvarchar(10),
	@goodsid		int,
	@skuid			int,
	@number			int
)
As
Begin
	

		
	Declare @ResultData nvarchar(max),@goods_list nvarchar(max),@order_total_price	numeric(10,2),@userid nvarchar(20)

	Select @userid = dbo.GetUserId(@token)
	If @type = 'dec'
		Update T_CartList Set Number = Number - 1 Where UserId = @userid And GoodsId = @goodsid And GoodsSkuId = @skuid 
	Else if @type = 'add'
		Update T_CartList Set Number = Number + @number Where UserId = @userid And GoodsId = @goodsid And GoodsSkuId = @skuid 
	Else if @type = 'delete'
		Delete From T_CartList Where UserId = @userid And GoodsId = @goodsid And GoodsSkuId = @skuid 
			
	Select @ResultData = '{}'
	Select @ResultData Result
End
Go

