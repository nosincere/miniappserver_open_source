IF OBJECT_ID('P_OpereateProductList') IS Not Null
	Drop Procedure P_OpereateProductList
Go
Create procedure P_OpereateProductList
(
	@Id			nvarchar(50),
	@Title		nvarchar(50),
	@ProPerty	nvarchar(10),
	@iorder		int,
	@bdisplay	bit,
	@outid		nvarchar(20) output
)
As
Begin
	If ISNULL(@Id,'')=''
	begin
		Insert Into T_Goods(Title,ProPerty,iOrder,bDisplay)
		select @Title,@ProPerty,@iorder,@bdisplay
		
		select @outid = @@IDENTITY
	end
	else
	begin
		update T_Goods set Title=@Title,ProPerty=@ProPerty,iOrder = @iorder,bDisplay = @bdisplay
		where Id=@Id
		
		select @outid = @Id
	end

End
Go