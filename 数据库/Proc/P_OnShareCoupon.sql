
--管理员转发优惠券
If OBJECT_ID('P_OnShareCoupon') Is Not Null
	Drop Procedure P_OnShareCoupon
Go
Create Procedure P_OnShareCoupon
(
	@RandomId	nvarchar(200),
	@couponid	int
)
AS
Begin
	Declare @allowmultishare nvarchar(10)
	Select @allowmultishare = SubValue From T_WxAppConfig Where SubKey='AllowMultiShareCoupon'
	
	Insert Into T_CouponShare(RandomId,CouponId,ShareType,IsGet)
	Select @RandomId,@couponid,@allowmultishare,0
End
Go