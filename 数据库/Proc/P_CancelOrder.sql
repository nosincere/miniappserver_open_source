IF OBJECT_ID('P_CancelOrder') IS Not Null
	Drop Procedure P_CancelOrder
Go
Create procedure P_CancelOrder
(
	@token		nvarchar(50),
	@orderid	int
)
As
Begin
	Select @token = dbo.GetUserId(@token)
	if not exists(select 1 from T_Order o 
			inner join T_WxUser u on o.UserId = u.[user_id]
			where u.[user_id] = @token
				And o.Id = @orderid)
	Begin
		Select '{"retcode":-500,"retmsg":"不能操作其他用户订单"}' Result
		return
	End

	--更新库存
	Update gd set KuCun = KuCun + od.Number
	From T_GoodsDetail gd
	Inner Join T_OrderDetail od on gd.Id = od.GoodsSkuId
	where od.OrderId = @orderid
	
	--回退优惠券
	If exists(select 1 from T_UserCoupon uc inner join T_Order o on o.UserCouponId = uc.Id where o.Id=@orderid)
	begin
		update uc set uc.IsUsed = 0
		from T_UserCoupon uc 
		inner join T_Order o on o.UserCouponId = uc.Id 
		where o.Id=@orderid
	end

	update T_Order set OrderStatus=-10,OrderStatusText='已取消' where Id=@orderid
	update T_OrderStatus set OrderStatus = -10,OrderStatusText='已取消',PayStatus=-10,PayStatusText='已取消' where OrderId=@orderid
	--Delete From T_Order where Id = @orderid
	--Delete From T_OrderDetail Where OrderId =@orderid
	--Delete From T_Delivery where OrderId = @orderid
	--Delete From T_OrderStatus where OrderId = @orderid
End
Go
