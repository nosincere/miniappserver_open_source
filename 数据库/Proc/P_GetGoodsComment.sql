If OBJECT_ID('P_GetGoodsComment') Is Not Null
	Drop Procedure P_GetGoodsComment
Go

Create Procedure P_GetGoodsComment
(
	@goodsid			nvarchar(200),
	@token			nvarchar(200)
)
As
Begin
		
	Declare @ResultData nvarchar(max),@userid nvarchar(20),@comment nvarchar(max)
	Select @userid = dbo.GetUserId(@token)

	
	
	Select u.avatarUrl,u.nickName,c.Stars,c.Comment,c.CreateTime,img.ImgUrl,gd.Name
	Into #comment
	From T_OrderDetailComment c
		Inner Join T_Goods g on c.GoodsId = g.Id
		Inner Join T_GoodsDetail gd on c.GoodsSkuId = gd.Id
		Inner Join T_WxUser u on c.WxUserId = u.[user_id]
		Left Join (SELECT OrderDetailId, STUFF(
			( SELECT ',"'+ ImgUrl +'"'
			FROM T_CommentImage b 
			WHERE b.OrderDetailId = a.OrderDetailId 
			FOR XML PATH('')),1 ,1, '') ImgUrl
			from T_CommentImage a
			group by OrderDetailId
			) img on c.OrderDetailId = img.OrderDetailId
		where c.GoodsId = @goodsid
		Order By c.CreateTime desc
	
	select @comment = ''
	select @comment = @comment + '{' +
			'"skuname":"'+Name+'",'+
			'"avatar":"'+avatarUrl+'",'+
			'"nickname":"'+nickName+'",'+
			'"stars":'+cast(Stars as nvarchar)+','+
			'"comment":"'+Comment+'",'+
			'"createtime":"'+CONVERT(varchar(100), CreateTime, 23) + ' ' + CONVERT(varchar(100), CreateTime, 108)+'",'+
			'"imgurl":['+ISNULL(ImgUrl,'')+']'+
		'},'
		From #comment
		
	IF LEN(@comment) > 0
		Select @comment = LEFT(@comment,LEN(@comment)-1)	
	Select @comment = ISNULL(@comment,'')
	
	
	Select @ResultData = '{"comment":['+@comment+']}'
	Select @ResultData Result
End
Go

--exec P_GetGoodsDetail 1
--select * from T_Goods
--select * from T_GoodsDetail