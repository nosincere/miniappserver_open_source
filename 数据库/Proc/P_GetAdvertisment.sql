
if exists (select * from dbo.sysobjects where id = object_id(N'P_GetAdvertisment'))
	drop procedure P_GetAdvertisment
GO

Create Procedure P_GetAdvertisment
As
Begin
	select * from T_Advertisement order by iOrder desc
End
Go

if exists (select * from dbo.sysobjects where id = object_id(N'P_AddAdvertisment'))
	drop procedure P_AddAdvertisment
GO

Create Procedure P_AddAdvertisment
(
	@name		nvarchar(200),
	@url		nvarchar(200),
	@picpath	nvarchar(200),
	@author		nvarchar(200),
	@order		int
)
As
Begin
	Create Table #ErrMsg(Code int,Msg nvarchar(200))
	if ISNULL(@picpath,'')=''
	begin
		insert into #ErrMsg(Code,Msg)
		select -1,'图片路径不能为空'
	end
	else
	begin
		insert into T_Advertisement(cName,cUrl,cPicPath,dAddTime,iOrder)
		select @name,@url,@picpath,GETDATE(),@order
	end
	select * from #ErrMsg
End
Go

if exists (select * from dbo.sysobjects where id = object_id(N'P_ModifyAdvertisment'))
	drop procedure P_ModifyAdvertisment
GO

Create Procedure P_ModifyAdvertisment
(
	@id			int,
	@name		nvarchar(200),
	@url		nvarchar(200),
	@picpath	nvarchar(200),
	@author		nvarchar(200),
	@order		int
)
As
Begin
	Create Table #ErrMsg(Code int,Msg nvarchar(200))
	if ISNULL(@picpath,'')=''
	begin
		insert into #ErrMsg(Code,Msg)
		select -1,'图片路径不能为空'
	end
	else
	begin
		update T_Advertisement set cName=@name,cUrl=@url,cPicPath=@picpath,dAddTime=GETDATE(),iOrder=@order
		where Id = @id
	end
	select * from #ErrMsg
End
Go

if exists (select * from dbo.sysobjects where id = object_id(N'P_DeleteAdvertisment'))
	drop procedure P_DeleteAdvertisment
GO

Create Procedure P_DeleteAdvertisment
(
	@id			int
)
As
Begin
	Create Table #ErrMsg(Code int,Msg nvarchar(200))
	if not exists(select 1 from T_Advertisement)
	begin
		insert into #ErrMsg(Code,Msg)
		select -1,'未找到广告'
	end
	else
	begin
		delete from T_Advertisement
		where Id = @id
	end
	select * from #ErrMsg
End
Go
