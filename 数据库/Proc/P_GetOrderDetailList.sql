IF OBJECT_ID('P_GetOrderDetailList') IS Not Null
	Drop Procedure P_GetOrderDetailList
Go
Create procedure P_GetOrderDetailList
(
	@pageSize	int,
	@pageIndex	int,
	@sort		nvarchar(500),
	@order		nvarchar(500),
	@filter		nvarchar(500),
	@orderid	int,
	@total		int output
)
As
Begin

	Select g.Title,gd.Name,d.Number,d.Price,d.Number * d.Price as Amount,
		--d.dAddTime ,
		CONVERT(varchar(100), d.dAddTime, 23)+ ' ' + CONVERT(varchar(100), d.dAddTime, 108) as dAddTime,
		gd.KuCun,
		os.OrderStatusText,os.ExpressNo,kd.Name as ExpressCompany,
		o.OrderNo,d.Id as OrderDetailId,os.Id as OrderStatusId,
		os.OrderStatus
	Into #temp
	From T_OrderDetail d
	Inner Join T_GoodsDetail gd on d.GoodsSkuId = gd.Id
	Inner Join T_Goods g on d.GoodsId = g.Id
	Inner Join T_OrderStatus os on d.Id = os.OrderDetailId
	Inner Join T_Order o on d.OrderId = o.Id
	Left Join T_KdConfig kd on os.ExpressCompany = kd.Code
	Where d.OrderId = @orderid
	order by d.Id
	
	declare @records int
	
	exec P_ToPage @TableName ='#temp',@searchField='Title,Name,ExpressNo,ExpressCompany',@searchString=@filter,@searchOper='in',@PageSize=@pageSize,@PageIndex=@pageIndex,
		@PageTotal=@records output,@RecordTotal=@total output, @OrderByField=@sort,@OrderByType=@order
End
Go
