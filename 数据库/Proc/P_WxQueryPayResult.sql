If OBJECT_ID('P_WxQueryPayResult') Is Not Null
	Drop Procedure P_WxQueryPayResult
Go
Create Procedure P_WxQueryPayResult
(
	@orderid int
)
AS
Begin
	Declare @appid nvarchar(50),@mchid nvarchar(50),@mchsecrect nvarchar(100),@out_trade_no nvarchar(50)
	
	Select @appid = AppId From T_MiniAppConfig where ConfigType='miniapp'
	Select @mchid = AppId,@mchsecrect = AppSecrect From T_MiniAppConfig where ConfigType='miniapppay'
	Select @out_trade_no = OrderNo From T_Order where Id = @orderid
	
	Select @appid appid,@mchid mchid,@mchsecrect as mchsecrect,@out_trade_no as out_trade_no
End
Go