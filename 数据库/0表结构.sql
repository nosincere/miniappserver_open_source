If Not Exists(select * from sys.databases where name = 'MiniApp')
	Create DataBase MiniApp
Go
Use MiniApp

IF OBJECT_ID('T_CompanyInfo') IS NULL
BEGIN
	CREATE TABLE T_CompanyInfo
	(
		NAME				NVARCHAR(200),
		LoginTitle			NVARCHAR(200),--登录页，浏览器选项卡title
		LoginName			NVARCHAR(200),--登录页，logo-name
		LoginSystemName		nvarchar(200),--登录页，系统名
		LoginIconUrl		nvarchar(200)--登录页，浏览器选项卡icon
	)
END
Go

truncate table T_CompanyInfo
Go

--insert into T_CompanyInfo(NAME,LoginTitle,LoginName,LoginSystemName,LoginIconUrl)
--select 'zyq','晨艺商城','','晨艺商城','favicon.ico'
insert into T_CompanyInfo(NAME,LoginTitle,LoginName,LoginSystemName,LoginIconUrl)
select 'master','艺顺翔名酒行','','艺顺翔名酒行','favicon.ico'


--首页多图广告
If Not Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].T_Advertisement'))                        
CREATE TABLE [dbo].T_Advertisement
(
   [Id]		int IDENTITY(1,1) NOT NULL,--自增长字段
   [cName]		[nvarchar](100)	 NOT NULL  DEFAULT(''),	--广告名称
   [cUrl]		[nvarchar](256)	 NOT NULL  DEFAULT(''),	--移动端链接
   [cPicPath]   [nvarchar](256)	 NOT NULL  DEFAULT(''),	--图片路径
   [dAddTime]		[datetime] NOT NULL,				--添加时间
   [cAuthor]    [nvarchar](100)	 NOT NULL  DEFAULT(''),	--添加人
   [iOrder] [int] NOT NULL  DEFAULT(0)					--排序
)
GO

--首页公告
If Not Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[T_Messages]'))                        
CREATE TABLE [dbo].[T_Messages]
(
   [Id]		int IDENTITY(1,1) NOT NULL,	--自增长字段
   [cTitle]		[nvarchar](100)	 NOT NULL  DEFAULT(''),		--公告名称
   [cPicPath]	[nvarchar](256)	 NOT NULL  DEFAULT(''),		--图片路径
   [cAuthor]    [nvarchar](100)	 NOT NULL  DEFAULT(''),		--发布人
   [cDescription] [nvarchar](max)	 NOT NULL  DEFAULT(''),	--图文详情
   [dAddTime]		[datetime] NOT NULL,					--添加时间  
   [dPublishTime]	[datetime],								--发布时间
   [iOrder] [int] NOT NULL  DEFAULT(0),						--排序权重
   [iState] [int] NOT NULL  DEFAULT(0)						--发布状态
)
GO  
--Drop Table T_Goods
--商品表
If Not Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[T_Goods]'))                        
CREATE TABLE [dbo].T_Goods
(
   Id	int identity(1,1)   NOT NULL,				
   Title			nvarchar(200),--
   Property			nvarchar(20),--属性。自营、天猫等
   [iOrder] [int] NOT NULL  DEFAULT(0),			--排序权重
   [bDisplay] [bit] NOT NULL DEFAULT (0),		--是否上架（0下架   1上架）
   [dAddTime] [datetime] not null Default GetDate(),				--添加时间
   [dDisplayTime] [datetime] Default GetDate(),					--上架时间
   Content Nvarchar(max) Not Null DEFAULT'', 
   CategotyId int Not Null DEFAULT 0, --类别id 
   DefaultImgUrl nvarchar(20)	not null default 'noprice.jpg'
)
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'Content' AND [id] = OBJECT_ID('T_Goods')) 
	ALTER TABLE T_Goods ADD Content Nvarchar(max) Not Null DEFAULT'' 
GO

if not exists(SELECT * FROM syscolumns WHERE name = 'CategotyId' AND [id] = OBJECT_ID('T_Goods')) 
	ALTER TABLE T_Goods ADD CategotyId int Not Null DEFAULT'' 
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'DefaultImgUrl' AND [id] = OBJECT_ID('T_Goods')) 
	ALTER TABLE T_Goods ADD DefaultImgUrl nvarchar(20)	not null default 'noprice.jpg'
GO

If OBJECT_ID('T_GoodsBannerImage') is null
Create table T_GoodsBannerImage
(
	GoodsId	int,
	ImgUrl	nvarchar(200),
	iOrder int
)
Go
if not exists(SELECT * FROM syscolumns WHERE name = 'iOrder' AND [id] = OBJECT_ID('T_GoodsBannerImage')) 
	ALTER TABLE T_GoodsBannerImage ADD iOrder int not null default 0
GO

--Drop Table T_GoodsDetail
--商品属性价格
If Not Exists (Select * From dbo.sysobjects Where id = object_id(N'[dbo].[T_GoodsDetail]'))                        
CREATE TABLE [dbo].T_GoodsDetail
(
   Id	int identity(1,1)   NOT NULL,	
   GoodsId		int,--商品id			
   Name			nvarchar(20),--分类名称
   costprice	numeric(18,2),--原价
   Price		numeric(18,2),--正式价格，该价格用于计算
   KuCun		numeric(18,2),--库存数量
   ImgUrl		nvarchar(200),
   [iOrder] [int] NOT NULL  DEFAULT(0),			--排序权重
   [bDisplay] [bit] NOT NULL DEFAULT (0),		--是否上架（0下架   1上架）
   [dAddTime] [datetime] not null Default GetDate(),				--添加时间
   [dDisplayTime] [datetime] not null Default GetDate(),					--上架时间
)
GO

--收货地址
If Not Exists (Select * From dbo.sysobjects where id = object_id(N'[dbo].[T_ShoppingAddress]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [dbo].[T_ShoppingAddress](
    [Id]    [numeric](10, 0) IDENTITY(1,1) NOT NULL,        --自增长字段
    [cUserId] int NOT NULL DEFAULT 0,        --登录账号
    [cPerson] [nvarchar](100) NOT NULL DEFAULT(''), --联系人
	[cTelephone] [nvarchar](60) NOT NULL DEFAULT(''), --电话
	[cMobile] [nvarchar](20) NOT NULL DEFAULT(''), --手机号
	[cProvince] [nvarchar](20) NOT NULL DEFAULT(''), --省
	[cCity] [nvarchar](20) NOT NULL DEFAULT(''), --市
	[cCounty] [nvarchar](30) NOT NULL DEFAULT(''), --区
	[cAddress] [nvarchar](256) NOT NULL DEFAULT(''), --收货地址
	[cPostCode] [nvarchar](20) NOT NULL DEFAULT(''), --联系人
	[bDefault] [bit] NOT NULL DEFAULT (0), --是否默认地址
	[dAddTime] [datetime] not null default(getdate())
  )
GO


If Not Exists (Select * From dbo.sysobjects where id = object_id(N'[dbo].[T_MiniAppConfig]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin
	Create Table T_MiniAppConfig
	(
		AppId		nvarchar(200),
		AppSecrect	nvarchar(200),
		ConfigType	nvarchar(20)	--'miniapp','kdniao'
	)
End
Go

if not exists(SELECT * FROM syscolumns WHERE name = 'ConfigType' AND [id] = OBJECT_ID('T_MiniAppConfig')) 
	ALTER TABLE T_MiniAppConfig ADD ConfigType nvarchar(20) Not Null DEFAULT ''
GO
If Not Exists(Select 1 From T_MiniAppConfig)
Begin
	print '这里填写appid等'
	Insert Into T_MiniAppConfig(AppId,AppSecrect,ConfigType)
	--Select 'wx31f03c9a0310b235','826e6177a25d1fa0325600d6955b33e5','miniapp' union all
	Select 'wx31f118058d72380b','d26389309517235a35a32f31e7c37529','miniapp' union all
	Select '1644707','1d42b234-785b-4c71-ac77-0c3e0c0127ef','kdniao' union all
	Select '1603449883','QWERTYUIOPLKJHGFDSAZXCVBNMdyzsxy','miniapppay'
End
Go
	
if OBJECT_ID('T_LoginUser') is null
begin
	Create Table T_LoginUser
	(
		Id				int identity(1,1),
		UserCode		nvarchar(200),
		Name			nvarchar(200),
		[PassWord]		nvarchar(200),
		IsManager		int,
		UType			int,--用户类型
		WeChatOpenId	nvarchar(100)
	)
end
Go

If Not Exists(Select 1 From T_LoginUser)
Begin
	Insert Into T_LoginUser(UserCode,Name,PassWord,IsManager)
	Select 'master','','',1
End
Go

If OBJECT_ID('T_WxUser') Is Null
Begin
	Create Table T_WxUser
	(
		[user_id]	int identity(1,1),
		openid		nvarchar(200),
		nickName	nvarchar(200),
		gender		int,
		[language]	nvarchar(50),
		city		nvarchar(50),
		province	nvarchar(50),
		country		nvarchar(50),
		avatarUrl	nvarchar(400)
	)
End
Go

if not exists(SELECT * FROM syscolumns WHERE name = 'IsAdmin' AND [id] = OBJECT_ID('T_WxUser')) 
	ALTER TABLE T_WxUser ADD IsAdmin int Not Null DEFAULT 0
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'CreateTime' AND [id] = OBJECT_ID('T_WxUser')) 
	ALTER TABLE T_WxUser ADD CreateTime datetime Not Null default getdate()
GO

--小程序配置
If OBJECT_ID('T_WxAppConfig') Is Null
Begin
	Create Table T_WxAppConfig
	(
		SubKey		nvarchar(50),
		SubValue	nvarchar(100)
	)
End

If Not Exists (Select 1 From T_WxAppConfig Where SubKey='searchStyle')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'searchStyle','radius'
Go
If Not Exists (Select 1 From T_WxAppConfig Where SubKey='searchtextAlign')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'searchtextAlign','center'
Go	
If Exists (Select 1 From T_WxAppConfig Where SubKey='searchplaceholder' and SubValue='center')
	delete From T_WxAppConfig Where SubKey='searchplaceholder' and SubValue='center'
Go
If Not Exists (Select 1 From T_WxAppConfig Where SubKey='searchplaceholder')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'searchplaceholder','输入商品名称'
Go
If Not Exists (Select 1 From T_WxAppConfig Where SubKey='bannerbtnColor')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'bannerbtnColor','#f21212'
Go
If Not Exists (Select 1 From T_WxAppConfig Where SubKey='bannerbtnShape')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'bannerbtnShape','rectangle'
Go
--是否允许负库存，默认否
If Not Exists (Select 1 From T_WxAppConfig Where SubKey='AllowNegativeKucun')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'AllowNegativeKucun','0'
Go
--是否允许转发优惠券，默认否
If Not Exists (Select 1 From T_WxAppConfig Where SubKey='AllowMultiShareCoupon')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'AllowMultiShareCoupon','0'
Go

If Not Exists (Select 1 From T_WxAppConfig Where SubKey='WxHomePageShowNew')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'WxHomePageShowNew','0'
Go

If Not Exists (Select 1 From T_WxAppConfig Where SubKey='WxHomeNoticeContent')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'WxHomeNoticeContent',''
Go
--是否显示签到按钮
If Not Exists (Select 1 From T_WxAppConfig Where SubKey='WxMiniAppShowSignIn')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'WxMiniAppShowSignIn','1'
Go
--签到按钮默认背景颜色
If Not Exists (Select 1 From T_WxAppConfig Where SubKey='WxMiniAppSignInBackColor')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'WxMiniAppSignInBackColor','#bbf1df'
Go
--签到按钮默认字体颜色
If Not Exists (Select 1 From T_WxAppConfig Where SubKey='WxMiniAppSignInFontColor')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'WxMiniAppSignInFontColor','#666666'
Go

print '这里设置域名和IP'
--域名（微信统一下单通知地址）
If Not Exists (Select 1 From T_WxAppConfig Where SubKey='HostName')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'HostName','https://www.chengyi.xyz/dyzminiapp/'
Go
If Not Exists (Select 1 From T_WxAppConfig Where SubKey='IP')
	Insert Into T_WxAppConfig(SubKey,SubValue)
	Select 'IP','39.105.5.52'
Go

If Not Exists(Select 1 From T_WxAppConfig Where SubKey = 'CategoryLeveal')
	Insert Into T_WxAppConfig(SubKey, SubValue)
	Select 'CategoryLeveal', '2'
Go

--首页轮播图
If OBJECT_ID('T_WxAppBannerConfig') Is Null
Begin
	Create Table T_WxAppBannerConfig
	(
		Id			int identity(1,1),
		linkUrl		nvarchar(50),
		imgUrl		nvarchar(400),
		GoodsId		int Not Null DEFAULT 0
	)
End
Go

if not exists(SELECT * FROM syscolumns WHERE name = 'GoodsId' AND [id] = OBJECT_ID('T_WxAppBannerConfig')) 
	ALTER TABLE T_WxAppBannerConfig ADD GoodsId int Not Null DEFAULT 0
GO


--If Not Exists (Select 1 From T_WxAppBannerConfig)
--	Insert Into T_WxAppBannerConfig(linkUrl,imgUrl,GoodsId)
--	Select 'pages/goods/index','http://www.baidu.com/img/baidu_jgylogo3.gif',1 Union All
--	Select 'pages/goods/index','http://www.baidu.com/img/baidu_jgylogo3.gif',2 Union All
--	Select 'pages/goods/index','http://www.baidu.com/img/baidu_jgylogo3.gif',3
--Go

--商品分类
If OBJECT_ID('T_Category') Is Null
Begin
	Create Table T_Category
	(
		Id		int identity(1,1),
		Name	nvarchar(20),
		ParId	int,
		ImgUrl	nvarchar(200),
		GoodsIds	nvarchar(200),
		OrderIndex	int,
		Leveal		Int
	)
End
Go

if not exists(SELECT * FROM syscolumns WHERE name = 'OrderIndex' AND [id] = OBJECT_ID('T_Category')) 
	ALTER TABLE T_Category ADD OrderIndex int Not Null DEFAULT 0
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'GoodsIds' AND [id] = OBJECT_ID('T_Category')) 
	ALTER TABLE T_Category ADD GoodsIds nvarchar(200) Not Null DEFAULT ''
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'ShowInIndexPage' AND [id] = OBJECT_ID('T_Category')) 
	ALTER TABLE T_Category ADD ShowInIndexPage int Not Null DEFAULT 0
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'Leveal' AND [id] = OBJECT_ID('T_Category')) 
Begin
	ALTER TABLE T_Category ADD Leveal int Not Null DEFAULT 0
	Exec('Update T_Category Set Leveal = 1 Where ParId = 0')
	Exec('Update T_Category Set Leveal = 2 Where ParId <> 0')
End
GO

--购物车
If OBJECT_ID('T_CartList') Is Null
Begin
	Create Table T_CartList
	(
		id				int identity(1,1),
		UserId			int,
		GoodsId			int,
		GoodsSkuId		int,--对应T_GoodsDetail表id
		AddPrice		numeric(18,2),--加入购物车时的价格
		Number			int,
		Checked			bit	not null default 1	--是否选中，使用购物车提交订单时用到
	)
End
Go

if not exists(SELECT * FROM syscolumns WHERE name = 'Checked' AND [id] = OBJECT_ID('T_CartList')) 
	ALTER TABLE T_CartList ADD Checked bit DEFAULT 1
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'id' AND [id] = OBJECT_ID('T_CartList')) 
	ALTER TABLE T_CartList ADD id int identity(1,1)
GO

--订单主表
If OBJECT_ID('T_Order') Is Null
Begin
	Create Table T_Order
	(
		Id				int identity(1,1),
		UserId			int,
		OrderNo			nvarchar(50),
		OrderStatus		int Not Null Default 0,			--回写回来的订单状态
		OrderStatusText	nvarchar(10) Not Null Default '',
		TotalAmount		numeric(18,2),	--总金额 包含物流
		DeliveryAmount	numeric(18,2),	--物流金额
		TotalNumber		int, 			--总数量
		[dAddTime] [datetime] not null default(getdate())
	)
End
Go
if not exists(SELECT * FROM syscolumns WHERE name = 'OrderStatus' AND [id] = OBJECT_ID('T_Order')) 
	ALTER TABLE T_Order ADD OrderStatus int Not Null DEFAULT 0
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'OrderStatusText' AND [id] = OBJECT_ID('T_Order')) 
	ALTER TABLE T_Order ADD OrderStatusText Nvarchar(10) Not Null DEFAULT'' 
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'CouponAmount' AND [id] = OBJECT_ID('T_Order')) 
	ALTER TABLE T_Order ADD CouponAmount numeric(18,2) Not Null DEFAULT 0 
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'UserCouponId' AND [id] = OBJECT_ID('T_Order')) 
	ALTER TABLE T_Order ADD UserCouponId int Not Null DEFAULT 0 
GO
--是否需要检查支付状态 0 不需要，1 需要，2 已检查
if not exists(SELECT * FROM syscolumns WHERE name = 'CheckPayResult' AND [id] = OBJECT_ID('T_Order')) 
	ALTER TABLE T_Order ADD CheckPayResult int Not Null DEFAULT 0 
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'Comment' AND [id] = OBJECT_ID('T_Order')) 
	ALTER TABLE T_Order ADD Comment nvarchar(400) Not Null DEFAULT ''
GO
--select * from T_Order select * from T_OrderDetail select * from T_OrderStatus
--delete from T_Order delete from T_OrderDetail delete from T_OrderStatus
--订单明细表（订单商品）
If OBJECT_ID('T_OrderDetail') Is Null
Begin
	Create Table T_OrderDetail
	(
		Id				int identity(1,1),
		OrderId			int,
		GoodsId			int,
		GoodsSkuId		int,			--对应T_GoodsDetail表id
		Price			numeric(18,2),	--单价
		Number			int,			--数量
		[dAddTime] [datetime] not null default(getdate())
	)
End
Go

--存放收货信息,一个订单一个收货地址
If OBJECT_ID('T_Delivery') Is Null
Begin
	Create Table T_Delivery
	(
		Id				int identity(1,1),
		OrderId			int,
		cPerson			nvarchar(10),
		cTelephone		nvarchar(20),
		cProvince		nvarchar(20),
		cCity			nvarchar(20),
		cCounty			nvarchar(20),
		cAddress		nvarchar(100),
		Total			numeric(18,2)
	)
End
Go


--订单状态表
If OBJECT_ID('T_OrderStatus') is Null
Begin
	Create table T_OrderStatus
	(
		Id					int identity(1,1),
		OrderId				int,			--订单id
		OrderDetailId		int,			--订单明细Id
		OrderStatus			int,			--订单状态	10,20,30,40
		OrderStatusText		nvarchar(20),	--订单状态
		PayStatus			int not null default 0,				--支付状态10,20
		PayStatusText		nvarchar(20) not null default '',	--支付状态
		DeliveryStatus		int not null default 0,				--发货状态10,20
		DeliveryStatusText	nvarchar(20) not null default '',	--发货状态
		ReceiptStatus		int not null default 0,				--收货状态10,20
		ReceiptStatusText	nvarchar(20) not null default '',	--收货状态
		CommentStatus		int not null default 0,
		CommentStatusText	nvarchar(20) not null default '',
		ExpressNo			nvarchar(50)			--物流单号
	)
End
Go

if not exists(SELECT * FROM syscolumns WHERE name = 'ExpressCompany' AND [id] = OBJECT_ID('T_OrderStatus')) 
	ALTER TABLE T_OrderStatus ADD ExpressCompany Nvarchar(50) Not Null DEFAULT'' 
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'CommentStatus' AND [id] = OBJECT_ID('T_OrderStatus')) 
	ALTER TABLE T_OrderStatus ADD CommentStatus int Not Null DEFAULT 0 
GO
if not exists(SELECT * FROM syscolumns WHERE name = 'CommentStatusText' AND [id] = OBJECT_ID('T_OrderStatus')) 
	ALTER TABLE T_OrderStatus ADD CommentStatusText Nvarchar(20) Not Null DEFAULT'' 
GO

--配送价格表
If OBJECT_ID('T_DeliveryPrice') Is Null
Begin
	Create Table T_DeliveryPrice
	(
		Province	nvarchar(10), --地区
		Price		numeric(18,2)--价格
	)
End
Go
--默认配送费10元
If Not Exists(Select 1 from T_DeliveryPrice where Province='默认')
	Insert Into T_DeliveryPrice(Province,Price) values('默认',0)
Go
--商品详情内用到文件
If OBJECT_ID('T_GoodsContentFiles') Is Null
Begin
	create Table T_GoodsContentFiles
	(
		GoodsId			int,
		ContentFileUrl	nvarchar(200)
	)
End
Go

If Not Exists(Select 1 From T_GoodsContentFiles Where GoodsId = 0)
	Insert Into T_GoodsContentFiles(GoodsId,ContentFileUrl) values(0,'noprice.jpg')

--使用到的文件表
--添加了新表必须在这里配置，不然会被删除
print('T_UsedFileConfig添加了新表必须在这里配置，不然会被删除')
IF OBJECT_ID('T_UsedFileConfig')	Is Null
Begin
	Create Table T_UsedFileConfig
	(
		TableName	nvarchar(50),
		ColumnName	nvarchar(50)
	)
End
Go

If OBJECT_ID('T_KdConfig') Is Null
Begin
	Create Table T_KdConfig
	(
		Id	int identity(1,1),
		Code	nvarchar(20),
		Name	nvarchar(20),
		[Default] int
	)
End
Go

if not exists(SELECT * FROM syscolumns WHERE name = 'Default' AND [id] = OBJECT_ID('T_KdConfig')) 
	ALTER TABLE T_KdConfig ADD [Default] int Not Null Default 0 
GO

Truncate Table T_KdConfig
Go

---快递鸟，免费的就这三个公司
Insert Into T_KdConfig(Code,Name,[Default])
Select 'STO','申通快递',0 Union All
Select 'YTO','圆通速递',0 Union All
Select 'ZTO','中通快递',0 Union All
Select 'Customer','线下配送',1 Union All
Select 'Other','其他',0
Go
--小程序内帮助页面内容
If OBJECT_ID('T_WxHelp') Is Null
Begin
	Create Table T_WxHelp
	(
		Title	nvarchar(100),
		Content	nvarchar(4000)
	)
End
Go

--优惠券
If OBJECT_ID('T_Coupon') Is Null
Begin
	Create Table T_Coupon
	(
		Id	int identity(1,1),
		Title	nvarchar(50),
		Amount	numeric(18,2),--面额
		TotalCount	int,--总数量
		RemainCount	int,--未领数量
		CreateDate	datetime not null default getdate(),--创建时间
		OverDate	datetime,--过期时间
		LimitAmount	numeric(18,2),--最低使用价格、满减阈值
		IsStopUsing	int not null default 0	--是否暂停使用，默认0
	)
End
Go
if not exists(SELECT * FROM syscolumns WHERE name = 'IsStopUsing' AND [id] = OBJECT_ID('T_Coupon')) 
	ALTER TABLE T_Coupon ADD IsStopUsing int Not Null DEFAULT 0
GO

--优惠券使用范围
If OBJECT_ID('T_CouponLimit') Is Null
Begin
	Create Table T_CouponLimit
	(
		CouponId	int,
		GoodsId		int,
		SkuId		int
	)
ENd
Go

--管理分享用，防止多次转发
If OBJECT_ID('T_CouponShare') Is Null
Begin
	Create Table T_CouponShare
	(
		RandomId	nvarchar(200),--小程序内转发时生产的随机id，领取的时候根据该id来判断是否领取
		CouponId	int,
		ShareType	int,		--什么配置情况下转发的，与T_WxAppConfig 内AllowMultiShareCoupon配置相关，如果为1，则不允许非管理员转发领取
		IsGet		int			--是否领取
	)
End
Go

if not exists(SELECT * FROM syscolumns WHERE name = 'UserId' AND [id] = OBJECT_ID('T_CouponShare')) 
	ALTER TABLE T_CouponShare ADD UserId int 
GO

--用户优惠券
If OBJECT_ID('T_UserCoupon') Is Null
Begin
	Create Table T_UserCoupon
	(
		Id	int identity(1,1),
		UserId	int,
		CouponId	int,
		IsUsed	int not null default 0
	)
End
Go

if not exists(SELECT * FROM syscolumns WHERE name = 'IsUsed' AND [id] = OBJECT_ID('T_UserCoupon')) 
	ALTER TABLE T_UserCoupon ADD IsUsed int Not Null DEFAULT 0
GO

If OBJECT_ID('T_OrderDetailComment') Is Null
Begin
	Create Table T_OrderDetailComment
	(
		Id	int identity(1,1),
		GoodsId			int,
		GoodsSkuId		int,
		OrderDetailId int,
		Stars			int,--星星
		Comment			nvarchar(2000),
		WxUserId		int,
		CreateTime		datetime not null default getdate()
	)
End
Go

If OBJECT_ID('T_CommentImage') Is Null
Begin
	Create Table T_CommentImage
	(
		Id	int identity(1,1),
		OrderDetailId	int,
		ImgUrl	nvarchar(100),
		iOrder	int
	)
End
Go

If OBJECT_ID('T_Log') Is Null
Begin
	Create Table T_Log
	(
		Id int identity(1,1),
		CreateTime	datetime not null default getdate(),
		LogType	nvarchar(20),--日志级别
		[Function] nvarchar(50),--方法
		RequestPara	nvarchar(max),--请求参数
		ResponseData	nvarchar(max),--返回data，一般是json
		ErrMsg		nvarchar(max),--异常信息
		StackTrace	nvarchar(max)--堆栈
	)
End
Go

truncate table T_UsedFileConfig
go

Insert into T_UsedFileConfig(TableName,ColumnName)
Select 'T_GoodsDetail','ImgUrl' union all
Select 'T_WxAppBannerConfig','ImgUrl' union all
Select 'T_Category','ImgUrl' union all
select 'T_GoodsContentFiles','ContentFileUrl' union all
select 'T_GoodsBannerImage','ImgUrl' union all
select 'T_CommentImage','ImgUrl'
Go


If OBJECT_ID('T_BargainPrice') Is Null
Begin
	Create Table T_BargainPrice
	(
		Id		int,
		GoodsId	int,--商品id，这里只根据商品id来，不加属性了
		StartTime	datetime,
		EndTime		datetime,
		TargetPrice	numeric(18,2), --目标金额，砍价到该金额即可购买，默认应该是商品金额
		MinPrice	numeric(18,2), --每次砍价最低价
		MaxPrice	numeric(18,2), --每次砍价最高价
	)
End
Go


If OBJECT_ID('T_GroupBuy') Is Null
Begin
	Create Table T_GroupBuy
	(
		Id		int,
		GoodsId	int,--商品id，这里只根据商品id来，不加属性了
		StartTime	datetime,
		EndTime		datetime,
		TargetPrice	numeric(18,2), --目标金额，砍价到该金额即可购买，默认应该是商品金额
		MinPrice	numeric(18,2), --每次砍价最低价
		MaxPrice	numeric(18,2), --每次砍价最高价
	)
End
Go

If OBJECT_ID('T_WxMiniAppUserPoiontsLog') Is Null
Begin
	Create Table T_WxMiniAppUserPoiontsLog
	(
		Id				int identity(1,1),
		UserId			int,
		[Type]			nvarchar(10),--sign	order
		Points			int,--可正可负
		SignDate		nvarchar(10),--type 为签到（sign）时 签到日期
		CreateDate		nvarchar(10) not null Default CONVERT(varchar(100), GETDATE(), 23),
		RelationId		int not null default 0,--type 为订单（order）时关联订单id，用于点击跳转到订单详情
		Comment			nvarchar(200) Not Null
	)
End
Go

If OBJECT_ID('T_WxMiniAppUserPoints') Is Null
Begin
	Create Table T_WxMiniAppUserPoints
	(
		Id		int identity(1,1),
		UserId	int,
		Points	int	--当前积分
	)
End
Go


