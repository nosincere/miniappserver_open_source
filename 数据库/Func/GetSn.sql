if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].GetSn') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].GetSn
GO

CREATE FUNCTION GetSn
(
	@index int
)
RETURNS NVARCHAR(max)
AS
BEGIN
	Declare @Sn nvarchar(max),@indexstr nvarchar(10)
	SELECT @Sn = replace(replace(replace(replace(CONVERT(varchar, getdate(), 121 ),'-',''),' ',''),':',''),'.','')
	Select @indexstr = CAST(@index as nvarchar)
	While(LEN(@indexstr) < 5)
	Begin
		select @indexstr = '0' + @indexstr
	End
	Select @Sn = @Sn + @indexstr
	Return @Sn
END
GO