if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].GetUserId') and xtype in (N'FN', N'IF', N'TF'))
	drop function [dbo].GetUserId
GO

CREATE FUNCTION GetUserId
(
	@token nvarchar(50)
)
RETURNS int
AS
BEGIN
	declare @userid int
	select @userid = [user_id] from T_WxUser where RIGHT(sys.fn_varbintohexstr(HASHBYTES('MD5','!((#token' + CAST([user_id] as nvarchar) )),32) = @token
	return @userid
END
GO

