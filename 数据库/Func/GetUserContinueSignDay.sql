if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].GetUserContinueSignDay') and xtype in (N'FN', N'IF', N'TF'))
	drop function [dbo].GetUserContinueSignDay
GO

CREATE FUNCTION GetUserContinueSignDay
(
	@token nvarchar(50)
)
RETURNS int
AS
BEGIN
	Declare @days int,@enddate nvarchar(10)
	If Not Exists(Select 1 From T_WxMiniAppUserPoiontsLog WHere UserId =@token And SignDate = CONVERT(varchar(100), GETDATE(), 23))
		Select @enddate = CONVERT(varchar(100), dateadd(day,-1,GETDATE()),23)
	else
		Select @enddate = CONVERT(varchar(100), GETDATE(), 23)
	
	;with cte 
	as (
		select UserId,SignDate from T_WxMiniAppUserPoiontsLog where UserId = @token And SignDate = @enddate And [Type]='sign'
		Union All
		Select a.UserId,a.SignDate From T_WxMiniAppUserPoiontsLog a Inner Join cte c on convert(datetime,a.SignDate) = dateadd(day,-1,convert(datetime,c.SignDate)) where a.UserId = @token And a.[Type]='sign'
	)

	select @days = COUNT(*) from cte	
	return @days
END
GO
