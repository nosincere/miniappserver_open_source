If OBJECT_ID('f_GetStrArrayStrOfIndex') is not null
	drop function f_GetStrArrayStrOfIndex
Go
CREATE function f_GetStrArrayStrOfIndex
(
	@str varchar(1024), --要分割的字符串
	@split varchar(10), --分隔符号
	@index int --取第几个元素 从1开始，如果是-1就取最后一个
)
returns varchar(1024)
as
begin
	declare @location int
	declare @start int
	declare @next int
	declare @seed int 
	declare @lastlocation int
	declare @result nvarchar(200)
	set @str=ltrim(rtrim(@str))
	set @start=1
	set @next=1
	set @seed=len(@split) 
	set @location=charindex(@split,@str)
	while @location<>0 and @index>@next
	begin   
		set @start=@location+@seed   
		set @location=charindex(@split,@str,@start)  
		set @next=@next+1
	end
	if @location =0 
		select @location =len(@str)+1 --这儿存在两种情况：1、字符串不存在分隔符号 2、字符串中存在分隔符号，跳出while循环后，@location为0，那默认为字符串后边有一个分隔符号。 
	if @index = -1
	begin
		if charindex (@split,@str) = 0
			select @result = @str
		else
		begin
			select @lastlocation = charindex (@split,reverse(@str), 1) 
			select @result = substring (@str,LEN(@str) - @lastlocation +2, 200) 
		end
	end
	else
		select @result =  substring(@str,@start,@location-@start)
	return @result 
end
Go
